<?php

include_once(__DIR__ . '/ModeloPrincipal.php');

class ModeloPaciente
{



  public $modeloP;


  public function __construct()
  {
    $this->modeloP = new ModeloPrincipal();
  }

  // Funcion para listar todos los pacientes segun Id encargado
  public function listarPacienteModelo($idEncargado)
  {
    $db = $this->modeloP->conectar();

    $query =   sprintf(
      "SELECT p.nombres,p.apellidos,p.direccion,p.telefono,
       p.numeroDocumento,p.fechaNacimiento,up.fechaRegistro,p.edad,p.estado
       FROM usu_paciente AS up
       INNER JOIN paciente AS p ON up.id_paciente = p.id
       WHERE up.id_encargado ='%s'",
      mysql_real_escape_string($idEncargado)
    );

    $data = mysql_query($query, $db);

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    return $data;
  }
  // funcion para buscar al paciente en el registro
  public function buscarPaciente($documento)
  {
    $db = $this->modeloP->conectar();

    $query =   sprintf(
      "SELECT * FROM paciente WHERE numeroDocumento ='%s'",
      mysql_real_escape_string($documento)
    );

    $data = mysql_query($query, $db);


    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }
    // comprobar si trae datos
    if ((mysql_num_rows($data)) != 0) {
      $usuario = mysql_fetch_array($data);
      $id_usuario = $usuario[0];



      $query2 = sprintf(
        "SELECT   u.nombres,u.apellidos
      FROM usu_paciente AS up
      INNER JOIN usuario AS u ON  up.id_encargado= u.id
      WHERE up.id_paciente= '%s'",
        mysql_real_escape_string($id_usuario)
      );
      $data2 = mysql_query($query2, $db);

      if (!$data2) {
        echo "Error de BD, no se pudo consultar la base de datos\n";
        echo "Error MySQL: " . mysql_error();
        exit;
      }

      if ((mysql_num_rows($data2)) != 0) {
        //cnxcncnkcnxkcn
        // Cuando el paciente tiene un terapeuta

        echo '<div class="row"> ';

        echo '<div class="col-md-3 col-xs-6 b-r"> <strong>Nombres y Apellidos</strong>';
        echo '<br>';
        echo '<p class="text-muted">' . $usuario[1] . '  ' . $usuario[2] . ' </p>';
        echo '</div>';

        echo '<div class="col-md-2 col-xs-6 b-r"> <strong>Direccion</strong>';
        echo '<br>';
        echo '<p class="text-muted"> ' . $usuario[3] . '</p>';
        echo '</div>';


        echo '<div class="col-md-2 col-xs-6 b-r"> <strong>Telefono y Numero documento</strong>';
        echo '<br>';
        echo '<p class="text-muted">' . $usuario[4] . '--- ' . $usuario[5] . '</p>';
        echo '</div>';

        echo '<div class="col-md-2 col-xs-6"> <strong>Fecha de nacimiento y Edad</strong>';
        echo '<br>';
        echo '<p class="text-muted">' . $usuario[6] . '----' . $usuario[7] . '</p>';
        echo '</div>';

        echo '<div class="col-md-2 col-xs-6"> <strong>Estado</strong>';
        echo '<br>';
        echo '<p class="text-muted">' . $usuario[8] . ' </p>';
        echo '</div>';

        echo '</div> <hr>';

        while ($row2 = mysql_fetch_array($data2)) {
          echo  '<div class="card-group">';
          echo  '<div class="card">';
          echo  '<div class="card-body">';
          echo  '<div class="row">';
          echo  '<div class="col-md-12">';
          echo  '<div class="d-flex no-block align-items-center">';
          echo  '<div>';
          echo  '<h3><i class=" ti-user"></i></h3>';
          echo  '<h2 class="text-muted">Terapeuta</h2>';
          echo  '<p class="text-muted">' . $row2['nombres'] . '<br>' . $row2['apellidos'] . '</p>';
          echo  '</div>';
          echo  '<div class="ml-auto">';
          echo  '<h2 class="counter text-primary"></h2>';
          echo  '</div>';
          echo  '</div>';
          echo  '</div>';
          echo  '<div class="col-12">';
          echo  '<div class="progress">';
          echo  '<div class="progress-bar bg-primary" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>';
          echo  '</div>';
          echo  '</div>';
          echo  '</div>';
          echo  '</div>';
          echo  '</div>';
          echo  '</div>';
        };
      } else {
        //cuando el paciente no tiene terapeuta
        echo '<div class="row"> ';

        echo '<div class="col-md-3 col-xs-6 b-r"> <strong>Nombres y Apellidos</strong>';
        echo '<br>';
        echo '<input type="hidden" id="idPac" value="' . $usuario[0] . '">';
        echo '<p class="text-muted">' . $usuario[1] . '  ' . $usuario[2] . ' </p>';
        echo '</div>';

        echo '<div class="col-md-2 col-xs-6 b-r"> <strong>Direccion</strong>';
        echo '<br>';
        echo '<p class="text-muted"> ' . $usuario[3] . '</p>';
        echo '</div>';


        echo '<div class="col-md-2 col-xs-6 b-r"> <strong>Email</strong>';
        echo '<br>';
        echo '<p class="text-muted">' . $usuario[4] . ' ' . $usuario[5] . '</p>';
        echo '</div>';

        echo '<div class="col-md-2 col-xs-6"> <strong>Disease</strong>';
        echo '<br>';
        echo '<p class="text-muted">' . $usuario[6] . ' ' . $usuario[7] . '</p>';
        echo '</div>';

        echo '<div class="col-md-2 col-xs-6"> <strong>Nuevo</strong>';
        echo '<br>';
        echo '<p class="text-muted">' . $usuario[8] . ' </p>';
        echo '</div>';

        echo '</div> <hr>';
        echo '<button class="btn btn-success" onclick="agregarPacTerap()"> Agregar</button>';
      }
    } else {
      // este uno indica a la funcion que no existe ningun registro
      // Lo validamos en sucess registrarBuscarpaciente  en javascript
      echo 1;
    }
  }
  // funcion para buscar al paciente en el registro
  public function buscarUsuarioValoracion($documento, $terapeuta)
  {
    $db = $this->modeloP->conectar();

    $query =   sprintf(
      "SELECT * FROM paciente WHERE numeroDocumento ='%s'",
      mysql_real_escape_string($documento)
    );

    $data = mysql_query($query, $db);


    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }
    // comprobar si trae datos

    if ((mysql_num_rows($data)) != 0) {
      $usuario = mysql_fetch_array($data);
      $id_usuario = $usuario[0];



      $query2 = sprintf(
        "SELECT   u.nombres,u.apellidos
      FROM usu_paciente AS up
      INNER JOIN usuario AS u ON  up.id_encargado= u.id
      WHERE up.id_paciente= '%s' AND  up.id_encargado= '%s' ",
        mysql_real_escape_string($id_usuario),
        mysql_real_escape_string($terapeuta)

      );
      $data2 = mysql_query($query2, $db);

      if (!$data2) {
        echo "Error de BD, no se pudo consultar la base de datos\n";
        echo "Error MySQL: " . mysql_error();
        exit;
      }

      if ((mysql_num_rows($data2)) != 0) {
        //cnxcncnkcnxkcn
        // Cuando el paciente tiene un terapeuta

        echo '<div class="row"> ';

        echo '<div class="col-md-3 col-xs-6 b-r"> <strong>Nombres</strong>';
        echo '<br>';
        echo '<p class="text-muted">' . $usuario[1] . '  ' . $usuario[2] . ' </p>';
        echo '</div>';

        echo '<div class="col-md-2 col-xs-6 b-r"> <strong>Direccion</strong>';
        echo '<br>';
        echo '<p class="text-muted"> ' . $usuario[3] . '</p>';
        echo '</div>';


        echo '<div class="col-md-2 col-xs-6 b-r"> <strong>Telefono y Numero de documento</strong>';
        echo '<br>';
        echo '<p class="text-muted">' . $usuario[4] . ' -----' . $usuario[5] . '</p>';
      
        echo '</div>';

        echo '<div class="col-md-2 col-xs-6"> <strong>Fecha Nacimiento y Edad</strong>';
        echo '<br>';
        echo '<p class="text-muted">' . $usuario[6] . ' ------' . $usuario[7] . '</p>';
        echo '</div>';

        echo '<div class="col-md-2 col-xs-6"> <strong>Estado</strong>';
        echo '<br>';
        echo '<p class="text-muted">' . $usuario[8] . ' </p>';
        echo '</div>';

        echo '</div> <hr>';
      } else {
        //cuando el paciente no le pertenece al terapeuta
        echo 2;
      }
    } else {
      // este uno indica a la funcion que no existe ningun registro
      // Lo validamos en sucess registrarBuscarpaciente  en javascript
      echo 1;
    }
  }



  // Funcion para registrar los pacientes
  public function regModeloPaciente($datos)
  {

    $json = array();
    session_start();
    $estado = 'Activo';
    $db = $this->modeloP->conectar();

    $query =   sprintf(
      "SELECT * FROM paciente WHERE numeroDocumento ='%s'",
      mysql_real_escape_string($datos['ndocumentoR'])
    );

    $data = mysql_query($query, $db);

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }
    // comprobar si trae datos
    if ((mysql_num_rows($data)) != 0) {
      echo "1"; // si exites el documento 
      exit;
    }


    // if($db->connect_error){
    //   die("La conexión ha fallado, error número " . $db->connect_errno . ": " . $db->connect_error);
    //  }
    // siempre utilizar el sprintf por 5.4 php

    $query2 = sprintf(
      "INSERT INTO paciente ( nombres, apellidos, direccion, telefono, numeroDocumento, fechaNacimiento, edad, estado) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s')",

      mysql_real_escape_string($datos['nombreR']),
      mysql_real_escape_string($datos['apellidoR']),
      mysql_real_escape_string($datos['direccionR']),
      mysql_real_escape_string($datos['telefonoR']),
      mysql_real_escape_string($datos['ndocumentoR']),
      mysql_real_escape_string($datos['fnacimientoR']),
      mysql_real_escape_string($datos['edadR']),
      mysql_real_escape_string($estado)
    );




    // $stmt = $db->prepare($query);
    $stmt = mysql_query($query2, $db);

    // $stmt->execute();
    if (!$stmt) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }
    $idPaciente = mysql_insert_id();
    date_default_timezone_set('America/Bogota');
    $hora = date('Y-m-d H:i:s');

    $query3 = sprintf(
      "INSERT INTO usu_paciente ( id_paciente, id_encargado,fechaRegistro) VALUES ('%s','%s','%s')",

      mysql_real_escape_string($idPaciente),
      mysql_real_escape_string($datos['idEncargado']),
      mysql_real_escape_string($hora)


    );
    $stmt4 = mysql_query($query3, $db);


    if (!$stmt4) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

     if(mysql_fetch_assoc( $stmt4)>0){
      $result = 'exito';
    }else{
      $result ='';
    }


    return $result;
  }


  // funcion agregar paciente al terapeuta todavia me falta ire a dormir despues de guardar
  public function agregarPacTerap($datos)
  {

    // hay algo mal 
    $db = $this->modeloP->conectar();
    // =========================
    //	Busca si el paciente ya esta registrado el el terapeuta que lo quiere agregar
    // ==============================
    $query = sprintf(
      " SELECT * FROM usu_paciente WHERE id_paciente = '%s' AND id_encargado ='%s'",
      //pasar las variables  por referencia
      mysql_real_escape_string($datos['idPac']),
      mysql_real_escape_string($datos['idEncargado'])
    );

    // ejecuta la query $query
    $data = mysql_query($query, $db);

    // Comprueba si fallo la consulta, si no hay errores continua
    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    // =========================
    //	Si la consulta $data trae algun registro implementar algun metrodo 
    // para evitar que continue ejecutando.
    // ==============================



    if ((mysql_num_rows($data)) != 0) {
      var_dump("holaaaaaaaaaaaaaaaaaa entreeeeeeee");
      echo "Ya existe, no se puede agregarggggg";
    } else {

      var_dump("SI PASOPOOOOOOOOOOOOOOOOOOOOOOOO");
      $query2 = sprintf(
        "INSERT INTO usu_paciente ( id_paciente, id_encargado) VALUES ('%s','%s')",

        mysql_real_escape_string($datos['idPac']),
        mysql_real_escape_string($datos['idEncargado'])
      );

      var_dump($query2);
      $data2 = mysql_query($query2, $db);
      var_dump($data2);

      if (!$data2) {
        echo "Error de BD, no se pudo consultar la base de datos\n";
        echo "Error MySQL: " . mysql_error();
        exit;
      }
    }

    return "paciente registrado con el terapeuta";
  }

  // Funcion  buscarDatosPaciente  buscar los datos


  public function buscarDatosPaciente($documento)
  {
    $db = $this->modeloP->conectar();

    $query =   sprintf(
      "SELECT * FROM paciente WHERE numeroDocumento ='%s'",
      mysql_real_escape_string($documento)
    );
    $data = mysql_query($query, $db);


    // Comprueba si fallo la consulta, si no hay errores continua
    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    if ((mysql_num_rows($data)) != 0) {
      // $paciente = mysql_fetch_array($data);
      $paciente = mysql_fetch_assoc($data);
      // $id_paciente = $paciente[0];

      echo json_encode([
        "opcion" => 1,
        "data" => $paciente
      ]);
    } else {

      echo json_encode([
        "opcion" => 2,
        "data" => " "
      ]);
    }
  }

  // Modificar los datos   del paciente










  // Esta funcion buscar, es la de consultar creeria yo
  public function buscarUsuariosModelo()
  {

    $json = array();
    $db = $this->modeloP->conectar();


    if ($db->connect_error) {
      die("La conexión ha fallado, error número " . $db->connect_errno . ": " . $db->connect_error);
    }

    $query = sprintf(" SELECT  nombres, apellidos, direccion, telefono, numeroDocumento, email, tipoU FROM usuario  WHERE tipoU != '1' AND tipoU != '2' ");

    // $stmt = $db->prepare($query);
    $stmt = mysql_query($query, $db);
    //$stmt->execute();

    $stmt->bind_result($nombres, $apellidos, $direccion, $telefono, $documento, $email, $tipoU);


    while ($stmt->fetch()) {

      if ($tipoU == 1) {
        $tipo = 'Admin';
      } else if ($tipoU == 2) {

        $tipo = 'Activo';
      } else if ($tipoU == 3) {
        $tipo = 'Pre-Registro';
      }



      $json[] = array(

        'nombres' => $nombres,
        'apellidos' => $apellidos,
        'direccion' => $direccion,
        'telefono' => $telefono,
        'documento' => $documento,
        'email' => $email,
        'tipoU' => $tipo

      );
    }


    $result = $json;
    return $result;
  }



  public function regModeloUsuarioP($datos, $fecha)
  {

    $json = array();

    $tipo = '3';


    $db = $this->modeloP->conectar();


    if ($db->connect_error) {
      die("La conexión ha fallado, error número " . $db->connect_errno . ": " . $db->connect_error);
    }

    $query = "INSERT INTO usuario (usuario, contra, nombres, apellidos, direccion, telefono, numeroDocumento, email, fechaNacimiento, fechaRegistro, tipoU) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

    $stmt = $db->prepare($query);
    $stmt->bind_param('sssssssssss', $datos['email'], $datos['documento'], $datos['nombres'], $datos['apellidos'],  $datos['direccion'],  $datos['telefono'], $datos['documento'], $datos['email'], $datos['fnacimiento'], $fecha, $tipo);
    $stmt->execute();


    if (($stmt->affected_rows) > 0) {
      $result = $stmt->insert_id;
    } else {
      $result = $stmt->insert_id;
    }


    return $result;
  }


  public function actEstadoPacienteModelo($datos)
  {


    $db = $this->modeloP->conectar();


    $query = sprintf(
      "UPDATE Paciente SET estado = '%s' WHERE numeroDocumento ='%s'",

      mysql_real_escape_string($datos['estado']),
      mysql_real_escape_string($datos['documento'])


    );

    $data = mysql_query($query, $db);

    var_dump($data);

    if ((mysql_fetch_assoc($data)) > 0) {
      $result = 'exito';
    } else {
      $result = '';
    }

    return $result;
  }


 


  //  Actualizar  datos del paciente

  public function actPacienteModelo($datos)
  {


    $db = $this->modeloP->conectar();

    $query =   sprintf(
      "UPDATE paciente 
    SET nombres= '%s',apellidos= '%s', direccion= '%s',
     telefono= '%s',fechaNacimiento= '%s',edad= '%s' 
     WHERE numeroDocumento ='%s'",
      mysql_real_escape_string($datos['nombreA']),
      mysql_real_escape_string($datos['apellidoA']),
      mysql_real_escape_string($datos['direccionA']),
      mysql_real_escape_string($datos['telefonoA']),
      mysql_real_escape_string($datos['fechaN']),
      mysql_real_escape_string($datos['edadA']),
      mysql_real_escape_string($datos['documento'])
    );

    var_dump($query);
    $data = mysql_query($query, $db);

    var_dump($data);

    // Comprueba si fallo la consulta, si no hay errores continua
    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

  

    return 1;
  }

  // Actualizar Datos del terapeuta

  public function ActualizarDatosTerapeuta($datos)
  {
    $db = $this->modeloP->conectar();

    $query =   sprintf(
      "UPDATE usuario 
    SET nombres='%s',apellidos= '%s', direccion= '%s',
     telefono='%s',email='%s', fechaNacimiento= '%s',contra='%s'  WHERE id ='%s'",
      mysql_real_escape_string($datos['nombres']),
      mysql_real_escape_string($datos['apellidos']),
      mysql_real_escape_string($datos['direccion']),
      mysql_real_escape_string($datos['telefono']),
      mysql_real_escape_string($datos['email']),
      mysql_real_escape_string($datos['fnacimiento']),
      mysql_real_escape_string($datos['contra']),
      mysql_real_escape_string($datos['ide'])
    );


    var_dump($query);
    $data = mysql_query($query, $db);

    var_dump($data);

    // Comprueba si fallo la consulta, si no hay errores continua
    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    exit;

    return 1;
  }




  public function actUsuarioModelo($datos)
  {

    session_start();
    $db = $this->modeloP->conectar();
    $id = $_SESSION['id'];

    if ($db->connect_error) {
      die("La conexión ha fallado, error número " . $db->connect_errno . ": " . $db->connect_error);
    }

    $query = "UPDATE Usuario SET nombres = ?,apellidos = ?,telefono =?, direccion= ?, contra =? WHERE id = ? ";

    $stmt = $db->prepare($query);
    $stmt->bind_param('sssssi', $datos['nombreA'], $datos['apellidoA'], $datos['telefonoA'], $datos['direccionA'], $datos['contra'], $id);
    $stmt->execute();

    if (($stmt->affected_rows) > 0) {
      $result = 'exito';



      $_SESSION['nombres'] = $datos['nombreA'];
      $_SESSION['apellidos'] = $datos['apellidoA'];
      $_SESSION['direccion'] = $datos['telefonoA'];
      $_SESSION['telefono'] = $datos['direccionA'];
    } else {
      $result = '';
    }


    return $result;
  }


  public function actUsuarioContraModelo($datos)
  {

    session_start();
    $db = $this->modeloP->conectar();


    if ($db->connect_error) {
      die("La conexión ha fallado, error número " . $db->connect_errno . ": " . $db->connect_error);
    }

    $query = "UPDATE Usuario SET contra =? WHERE usuario = ? AND numeroDocumento=?";

    $stmt = $db->prepare($query);
    $stmt->bind_param('sss', $datos['contra'], $datos['usuario'], $datos['documento']);
    $stmt->execute();

    if (($stmt->affected_rows) > 0) {
      $result = 'exito';
    } else {
      $result = '';
    }


    return $result;
  }




  
  public function buscarDatosEncargado($id)
  {
  
    $db = $this->modeloP->conectar();
  
    $query =   sprintf(
      "SELECT * FROM usuario WHERE id ='%s'",
      mysql_real_escape_string($id)
    );
    $data = mysql_query($query, $db);


    // Comprueba si fallo la consulta, si no hay errores continua
    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }


    
    if ((mysql_num_rows($data)) != 0) {
   
      $usuario = mysql_fetch_assoc($data);
  

      echo json_encode([
        "opcion" => 1,
        "data" => $usuario
      ]);
    } else {

      echo json_encode([
        "opcion" => 2,
        "data" => " "
      ]);
    }



  }


}
