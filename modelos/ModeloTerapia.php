<?php

include_once(__DIR__ . '/ModeloPrincipal.php');

class ModeloTerapia
{



  public $modeloP;


  public function __construct()
  {
    $this->modeloP = new ModeloPrincipal();
  }



  public function regModeloTerapia($datos, $fechaR)
  {

    $db = $this->modeloP->conectar();

    $avance = 0;
    $motivo = "Terapia";
    // var_dump($fechaR);

    $query =   sprintf(
      "SELECT * FROM paciente WHERE numeroDocumento ='%s'",
      mysql_real_escape_string($datos['documento'])
    );
    $data = mysql_query($query, $db);

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    $paciente = mysql_fetch_array($data);
    $estado = $paciente['estado'];

    if ($estado == 'Activo') {


      if ((mysql_num_rows($data)) != 0) {

        $paciente = mysql_fetch_assoc($data);

        $idP = $paciente['id'];

        // echo $idP;

        $query2 = sprintf(
          "INSERT INTO  terapia (idP, responsable, descripcion, fechaRegistro, fechaInicio, cantidadSesion, avance, estado)  VALUES ('%s','%s','%s','%s','%s','%s','%s','%s')",

          mysql_real_escape_string($idP),
          mysql_real_escape_string($datos['responsable']),
          mysql_real_escape_string($datos['descripcion']),
          mysql_real_escape_string($fechaR),
          mysql_real_escape_string($datos['fechaInicio']),
          mysql_real_escape_string($datos['cantSesion']),
          mysql_real_escape_string($avance),
          mysql_real_escape_string($estado)


        );

        $data2 = mysql_query($query2, $db);

        if (!$data2) {
          echo "Error de BD, no se pudo consultar la base de datos\n";
          echo "Error MySQL: " . mysql_error();
          exit;
        }

        $ultimo_id = mysql_insert_id($db);


        // var_dump($ultimo_id);


        $query3 = sprintf(
          "INSERT INTO historial(id_paciente,motivo,idMotivo,fechaRegistro) VALUES ('%s','%s','%s','%s')",

          mysql_real_escape_string($idP),
          mysql_real_escape_string($motivo),
          mysql_real_escape_string($ultimo_id),
          mysql_real_escape_string($fechaR)

        );


        $data3 = mysql_query($query3, $db);

        // //recibo el último id
        // $ultimo_id = mysql_insert_id($db);


        if (!$data3) {
          echo "Error de BD, no se pudo consultar la base de datos\n";
          echo "Error MySQL: " . mysql_error();
          exit;
        }


        if ($row = mysql_fetch_array($data3) > 0) {
          $result = $row->insert_id;
        } else {
          $result = 'm';
        }


      }

    } else{

        $result=0;
    }

  
   return $result;
  }

  public function regModeloSesion($datos, $fechaR)
  {

    $db = $this->modeloP->conectar();
    $estado = 'Activo';
    $motivo = 'Sesion';


    $query2 =   sprintf(

      "SELECT p.id,p.estado
      FROM paciente AS p
      INNER JOIN terapia AS t ON t.idP=p.id 
      WHERE t.id ='%s'",
      mysql_real_escape_string($datos['idT'])
    );


    $data2 = mysql_query($query2, $db);



    if (!$data2) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }



    $terapia = mysql_fetch_array($data2);
    $idP = $terapia['id'];
    $estado = $terapia['estado'];

    if ($estado == 'Activo') {

      $query1 = sprintf(
        "INSERT INTO  sesion(idT,responsable,descripcion,fechaRegistro)  VALUES ('%s','%s','%s','%s')",

        mysql_real_escape_string($datos['idT']),
        mysql_real_escape_string($datos['responsable']),
        mysql_real_escape_string($datos['des']),
        mysql_real_escape_string($fechaR)
      );

      $data1 = mysql_query($query1, $db);

      // var_dump($data1);

      if (!$data1) {
        echo "Error de BD, no se pudo consultar la base de datos\n";
        echo "Error MySQL: " . mysql_error();
        exit;
      }



      $ultimo_id = mysql_insert_id($db);





      $query3 = sprintf(
        "INSERT INTO historial(id_paciente,motivo,idMotivo,fechaRegistro) VALUES ('%s','%s','%s','%s')",

        mysql_real_escape_string($idP),
        mysql_real_escape_string($motivo),
        mysql_real_escape_string($ultimo_id),
        mysql_real_escape_string($fechaR)

      );


      $data3 = mysql_query($query3, $db);

      // //recibo el último id
      // $ultimo_id = mysql_insert_id($db);


      if (!$data3) {
        echo "Error de BD, no se pudo consultar la base de datos\n";
        echo "Error MySQL: " . mysql_error();
        exit;
      }

    

      if ($row = mysql_fetch_array($data3) > 0) {
        $result = $row->insert_id;
      } else {
        $result = 'm';
      }
   
    }else{
        $result=0;

    }

    return $result;

  }





 // Busca terapias para agregarlo en el select
  // Busca terapias para agregarlo en el select
  public function buscarTerapiasM($documento)
  {

    $db = $this->modeloP->conectar();
    $estado = 'Activo';
    $avance = 0;

    $query =   sprintf(
      "SELECT * FROM paciente WHERE numeroDocumento ='%s'",
      mysql_real_escape_string($documento)
    );
    $data = mysql_query($query, $db);

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }


    $paciente = mysql_fetch_assoc($data);

    $idP = $paciente['id'];
    $estado = $paciente['estado'];

    



    // Buscar las terapias  comparando id del paciente y idP
    if($estado == 'Activo'){
 

    $query2 =   sprintf(
      "SELECT t.id,t.responsable,
    t.fechaInicio
      FROM paciente AS p
      INNER JOIN terapia AS t ON t.idP=p.id 
      WHERE t.idP ='%s' AND t.estado='%s' ",

      mysql_real_escape_string($idP),
      mysql_real_escape_string($estado)
    );

    $data2 = mysql_query($query2, $db);


  
    $json = [];

    while ($fila = mysql_fetch_assoc($data2)) {

      $id = $fila['id'];
      $fecha = $fila['fechaInicio'];
      $res = $fila['responsable'];




      $jsonTemp[] = array(

        'idT'   => $id,
        'fecha' => $fecha,
        'res'   => $res

      );

      array_push($json, $jsonTemp);
    }

    $result = $json;
    return $result;
  }

  
  }


  public function buscarTerapiaM3($idT)
  {

    $db = $this->modeloP->conectar();
    $estado = 'Activo';

    $query2 =   sprintf(
      "SELECT t.id,t.idP,t.descripcion,t.responsable,
    t.fechaInicio,t.cantidadSesion,t.avance,t.estado
      FROM paciente AS p
      INNER JOIN terapia AS t ON t.idP=p.id 
      WHERE t.id ='%s'",

      mysql_real_escape_string($idT)
    );

    $data2 = mysql_query($query2, $db);

    // var_dump($data2);


    // printf($data2);

    if (!$data2) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    $json = [];

    while ($fila = mysql_fetch_assoc($data2)) {






      $jsonTemp[]  = array(

        'idT'            => $fila['id'],
        'idP'            => $fila['idP'],
        'descripcion'    => $fila['descripcion'],
        'res'            => $fila['responsable'],
        'fecha'          => $fila['fechaInicio'],
        'cantidadSesion' => $fila['cantidadSesion'],
        'avance'         => $fila['avance'],
        'estado'         => $fila['estado']


      );
      // el que almacena y el que va agregar
      array_push($json, $jsonTemp);
    }

    return $json;
  }



  public function actualizarTerapicaA($idT)
  {

    $db = $this->modeloP->conectar();

    $query =   sprintf(
      "UPDATE Terapia SET avance = avance+1 WHERE id = '%s' ",
      mysql_real_escape_string($idT)
    );
    $data = mysql_query($query, $db);

  

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

  
  }


  public function actualizarTerapicaE($idT)
  {

    $db = $this->modeloP->conectar();
    $estado = 'Finalizada';


    $query =   sprintf(
      "UPDATE Terapia SET estado ='%s' 
    WHERE avance = cantidadSesion  AND id = '%s' ",

      mysql_real_escape_string($estado),
      mysql_real_escape_string($idT)
    );
    $data = mysql_query($query, $db);

   

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

 
  }
}
