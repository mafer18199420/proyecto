<?php

include_once(__DIR__ . '/ModeloPrincipal.php');

class ModeloHistoria
{



  public $modeloP;


  public function __construct()
  {
    $this->modeloP = new ModeloPrincipal();
  }


  public function regHistoria($documento, $val, $fechaSistema)
  {

    var_dump($val);
    $db = $this->modeloP->conectar();


    $query =   sprintf(
      "SELECT  *  FROM paciente WHERE  numeroDocumento='%s' ",
      mysql_real_escape_string($documento)
    );

    $data = mysql_query($query, $db);

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    $paciente = mysql_fetch_array($data);
    $idP = $paciente['id'];




    $query2 =   sprintf(
      "SELECT  *  FROM tipovalora WHERE  id='%s' ",
      mysql_real_escape_string($val)
    );

    $data2 = mysql_query($query2, $db);

    if (!$data2) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    $tipovaloraw = mysql_fetch_array($data2);
    $nombreT = $tipovaloraw['nombre'];


    $query1 = sprintf(
      "INSERT INTO historial(id_paciente,motivo,idMotivo,fechaRegistro) VALUES ('%s','%s','%s','%s')",

      mysql_real_escape_string($idP),
      mysql_real_escape_string($nombreT),
      mysql_real_escape_string($val),
      mysql_real_escape_string($fechaSistema)

    );


    $data1 = mysql_query($query1, $db);

    //recibo el último id
    // $ultimo_id = mysql_insert_id($db);


    if (!$data1) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    if (($row = mysql_fetch_array($data2)) > 0) {
      $result = $row->insert_id;
    } else {
      $result = '';
    }


    return $result;
  }


  public function buscarHistoriaModelo($documento)
  {

    $db = $this->modeloP->conectar();

    $query =   sprintf(
      "SELECT  *  FROM paciente WHERE  numeroDocumento='%s' ",
      mysql_real_escape_string($documento)
    );

    $data = mysql_query($query, $db);

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    $paciente = mysql_fetch_array($data);
    $idP = $paciente['id'];


    $query2 =   sprintf(
      "SELECT p.nombres,p.apellidos,h.idMotivo,h.motivo,h.fechaRegistro
      FROM paciente AS p
      INNER JOIN historial AS h ON h.id_paciente=p.id 
      WHERE p.id ='%s'",

      mysql_real_escape_string($idP)
    );

    $data2 = mysql_query($query2, $db);



    // printf($data2);

    if (!$data2) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }



    $json = [];

    while ($fila = mysql_fetch_assoc($data2)) {


      $jsonTemp[] = array(

        'id' => $fila['idMotivo'],
        'nombres' => $fila['nombres'],
        'apellidos' => $fila['apellidos'],
        'motivo' => $fila['motivo'],
        'fecha' => $fila['fechaRegistro']



      );
      // el que almacena y el que va agregar
      array_push($json, $jsonTemp);
    }


    $result = json_encode($json);
    echo $result;
  }


  public function mostrarValoracion($data)
  {

    $db = $this->modeloP->conectar();
    var_dump($data[0]);

    $query =   sprintf(

      "SELECT p.id, p.pregunta,r.respuesta,r.id_recomendacion
      FROM preguntas AS p
      INNER JOIN respuesta AS r ON r.pregunta_id=p.id
      INNER JOIN valoracion AS v ON  v.idV=r.idV
      WHERE p.tipo_id ='%s' AND v.fechaValora ='%s'  ",
      mysql_real_escape_string($data[1]),
      mysql_real_escape_string($data[2])

    );

    // var_dump($query);
    $data  = mysql_query($query, $db);



    // printf($data2);

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }


    // $json = [];
    $indice = 0;
    while ($fila = mysql_fetch_assoc($data)) {

      $idP = $fila['id'];
      $pregunta = $fila['pregunta'];
      $idRecom = $fila['id_recomendacion'];



      // $jsonTemp[] = array(

      //   'pregunta' => $fila['pregunta'],
      //   'respuesta' => $fila['respuesta'],
      //   'idRecomendacion'=>$fila['id_recomendacion']



      // );


      $query1 =   sprintf(

        "SELECT re.descripcion
      FROM recomendacion AS re
      INNER JOIN respuesta AS r ON r.pregunta_id=re.idRe
      WHERE r.pregunta_id ='%s' AND r.id_recomendacion='%s' ",
        mysql_real_escape_string($idP),
        mysql_real_escape_string($idRecom)
      );


      $data1 = mysql_query($query1, $db);

      $fila2 = mysql_fetch_assoc($data1);


      echo " <tr>";
      echo " <td >" . htmlentities($fila['pregunta']) . "</td>";
      echo "<td  >" . htmlentities($fila2['descripcion']) . "</td>";
      echo "</tr>";
      $indice++;
    }
  }


  public  function mostrarTerapia($data)
  {


    $db = $this->modeloP->conectar();

    $query =   sprintf(
      "SELECT  *  FROM terapia WHERE  id='%s' AND  fechaRegistro='%s'  ",
      mysql_real_escape_string($data[1]),
      mysql_real_escape_string($data[2])
    );

    $data  = mysql_query($query, $db);



    // printf($data2);

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }
    var_dump($data);

    // $fila = mysql_fetch_assoc($data);

    // var_dump($fila);
    $indice = 0;
    while ($fila = mysql_fetch_assoc($data)) {

      var_dump($fila);

      echo " <tr>";
      echo " <td >" . utf8_encode($fila['responsable']) . "</td>";
      echo "<td  >" . utf8_encode($fila['descripcion']) . "</td>";
      echo "<td  >" . utf8_encode($fila['cantidadSesion']) . "</td>";
      echo "<td  >" . utf8_encode($fila['avance']) . "</td>";
      echo "<td  >" . utf8_encode($fila['estado']) . "</td>";
      echo "<td  >" . utf8_encode($fila['fechaInicio']) . "</td>";
      echo "</tr>";
      $indice++;
    }
  }

  public function mostrarSesion($data)
  {
     var_dump($data);
     $db = $this->modeloP->conectar();

     $query =   sprintf(
      "SELECT  *  FROM sesion WHERE  id='%s' AND  fechaRegistro='%s'  ",
      mysql_real_escape_string($data[1]),
      mysql_real_escape_string($data[2])
    );

    $data  = mysql_query($query, $db);



    // printf($data2);

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    $indice = 0;
    while ($fila = mysql_fetch_assoc($data)) {

      var_dump($fila);

      echo " <tr>";
      echo " <td >" . utf8_encode($fila['responsable']) . "</td>";
      echo "<td  >" . utf8_encode($fila['descripcion']) . "</td>";
      echo "<td  >" . utf8_encode($fila['fechaRegistro']) . "</td>";
      echo "</tr>";
      $indice++;
    }
    



  }





}
