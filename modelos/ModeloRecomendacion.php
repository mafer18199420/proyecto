<?php

include_once(__DIR__ . '/ModeloPrincipal.php');

class ModeloRecomendacion
{



  public $modeloP;


  public function __construct()
  {
    $this->modeloP = new ModeloPrincipal();
  }


  // Busca valoracion para agregarlo en el select
  public function buscarValoracion($documento, $terapeuta)
  {

    $db = $this->modeloP->conectar();


    $query =   sprintf(
      "SELECT * FROM paciente WHERE numeroDocumento ='%s'",
      mysql_real_escape_string($documento)
    );
    $data = mysql_query($query, $db);

    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }


    $paciente = mysql_fetch_assoc($data);

    $idP = $paciente['id'];


  

    // Buscar las terapias  comparando id del paciente y idP
    $query2 =   sprintf(

      "SELECT * FROM valoracion WHERE id_paciente ='%s' AND id_encargado ='%s' ",
      mysql_real_escape_string($idP),
      mysql_real_escape_string($terapeuta)
    );


    $data2 = mysql_query($query2, $db);



    if (!$data2) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }





  

    $json = array();

    while ($fila = mysql_fetch_assoc($data2)) {

      $id_pa = $fila['id_paciente'];
      $res = $fila['id_encargado'];
      $fecha = $fila['fechaValora'];
      $id_valoracion = $fila['idV'];

   

      $query3= sprintf(
        "SELECT p.tipo_id,t.nombre 
        FROM preguntas AS p
        INNER JOIN respuesta AS r ON r.pregunta_id=p.id
        INNER JOIN tipovalora AS t ON t.id=p.tipo_id
        WHERE r.idV ='%s' ",
        mysql_real_escape_string($id_valoracion)

      );

      
    $data3 = mysql_query($query3, $db);



    if (!$data3) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }



    $fila2 = mysql_fetch_assoc($data3);
    // var_dump($fila2);
    
      $jsonTemp2=array(

        'tipoId'=> $fila2['tipo_id'],
        'nombre'=> $fila2['nombre']
      );


   
      $jsonTemp = array(

        'idP'   => $id_pa,
        'fecha' => $fecha,
        'res'   => $res,
        'idV'   => $id_valoracion
      );


      $resultado = array_merge($jsonTemp2, $jsonTemp);

     

      array_push($json, $resultado);
    }



    $result = $json;


    return $result;
  }






  public function buscarRecomendacion($valoracion)
  {

    $db = $this->modeloP->conectar();

    // Me permite  hacer la consulta para sacar los datos como id de la pregunta,descripcion,tipo_id, respuesta,id_recomendacion
    $query =   sprintf(

      "SELECT p.id,p.pregunta,p.tipo_id ,r.respuesta,r.id_recomendacion
      FROM preguntas AS p
      INNER JOIN respuesta AS r ON r.pregunta_id=p.id
      WHERE r.idV ='%s' ",
      mysql_real_escape_string($valoracion)
    );




    $data = mysql_query($query, $db);



    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    // var_dump($data);

    // si cumple esta condicion hace las demas consultas , debe llegar algun dato
    if ((mysql_num_rows($data)) != 0) {


      $indice = 0;

      while ($fila = mysql_fetch_assoc($data)) {

        $id_tipo = $fila['tipo_id'];
        $id_pre = $fila['id'];
        $id_recomendacion = $fila['id_recomendacion'];


        // hace una consulta al tipo de valoracion para saber que nombre tiene con tipo_id que se trajo de anterior consulta
        $query2 =   sprintf(
          "SELECT * FROM tipovalora WHERE id ='%s'",
          mysql_real_escape_string($id_tipo)
        );
        $data2 = mysql_query($query2, $db);

        if (!$data2) {
          echo "Error de BD, no se pudo consultar la base de datos\n";
          echo "Error MySQL: " . mysql_error();
          exit;
        }


        $nombreValoracion = mysql_fetch_assoc($data2);

        // se les da un nombre a estos datos unos van hacer para la otra consulta
        $nombreV = $nombreValoracion['nombre'];



        // Se realiza esta consulta para traer la descripcion de la recomendacion según los datos sacados de la tabla respuesta

        $query1 =   sprintf(

          "SELECT re.descripcion
        FROM recomendacion AS re
        INNER JOIN respuesta AS r ON r.pregunta_id=re.idRe
        WHERE r.pregunta_id ='%s' AND r.id_recomendacion='%s' ",
          mysql_real_escape_string($id_pre),
          mysql_real_escape_string($id_recomendacion)
        );


        $data1 = mysql_query($query1, $db);

        $fila2 = mysql_fetch_assoc($data1);

        // se muesta los datos en los td de la tabla 


        echo " <tr>";
        echo  "<td >"  . $nombreV . "</td>";
        echo " <td >" . htmlentities($fila['pregunta']) . "</td>";
        echo "<td  >" .htmlentities($fila2['descripcion']) . "</td>";
        echo "</tr>";
        $indice++;
      }
    }
  }




  public function  buscarGrafica($valoracion)
  {
    $db = $this->modeloP->conectar();

    $query =   sprintf(
      "SELECT p.tipo_id ,r.respuesta,t.nombre
      FROM preguntas AS p
      INNER JOIN respuesta AS r ON r.pregunta_id=p.id
      INNER JOIN tipovalora AS t ON t.id =p.tipo_id
      WHERE r.idV ='%s'",
      mysql_real_escape_string($valoracion)
    );


    $data = mysql_query($query, $db);


    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

    $json = array();
    while ($fila = mysql_fetch_assoc($data)) {



      $jsonTemp = array(


        'res' => $fila['respuesta'],
        'tipo' => $fila['nombre']

      );

      array_push($json, $jsonTemp);
    }


    $result = $json;
    return $result;
  }



  



  public function  buscarGraficaGeneral($idC,$terapeuta)
  {

    $db = $this->modeloP->conectar();
  

    $query =   sprintf(
      "SELECT t.nombre,p.id,r.respuesta,v.id_paciente,v.id_encargado	
      FROM tipovalora AS t
      INNER JOIN preguntas AS p ON p.tipo_id=t.id
      INNER JOIN respuesta AS r ON r.pregunta_id=p.id
      INNER JOIN valoracion AS v ON v.idV=r.idV 
       WHERE t.id ='%s' AND v.id_encargado='%s'",
      mysql_real_escape_string($idC),
      mysql_real_escape_string($terapeuta)


    );


    $data = mysql_query($query, $db);
    // var_dump($data);


    if (!$data) {
      echo "Error de BD, no se pudo consultar la base de datos\n";
      echo "Error MySQL: " . mysql_error();
      exit;
    }

  

    $json = array();
    while ($fila2 = mysql_fetch_assoc($data)) {

       $res =$fila2['respuesta'];
       $nombreT=$fila2['nombre'];
       $idp=$fila2['id'];
       $idE=$fila2['id_encargado'];
       $idpac=$fila2['id_paciente'];

    

   
      $jsonTemp = array(


        'res' => $res,
        'tipo' => $nombreT,
        'id' => $idp,
        'idE' => $idE,
        'idP'=> $idpac


      );

      array_push($json, $jsonTemp);
    }


    $result = $json;
    return $result;
  }

}
