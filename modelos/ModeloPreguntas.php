<?php

include_once(__DIR__ . '/ModeloPrincipal.php');

class ModeloPreguntas
{



    public $modeloP;


    public function __construct()
    {
        $this->modeloP = new ModeloPrincipal();
    }


    // Funcion para listar todos los pacientes segun Id encargado
    public function listarPreguntas($val)
    {
        $db = $this->modeloP->conectar();

        $query =   sprintf(
            "SELECT  *  FROM preguntas WHERE  tipo_id='%s'  ORDER BY id ",
            mysql_real_escape_string($val)
        );

        $data = mysql_query($query, $db);

        if (!$data) {
            echo "Error de BD, no se pudo consultar la base de datos\n";
            echo "Error MySQL: " . mysql_error();
            exit;
        }

        if ((mysql_num_rows($data)) != 0) {
            //cnxcncnkcnxkcn
            // Cuando  trae algun dato
            $indice = 0;
            while ($row = mysql_fetch_array($data)) {

                echo " <tr>";
                echo  "<td style=display:none>" . $row['id'] . "</td>";
                echo " <td>" . htmlentities($row['pregunta']) . "</td>";
                echo " <td class= text-nowrap >";
                echo '<input type="radio" class="resRadio " id="pruebaSi" onclick="mostrarModal(' . $row['id'] . ', 1)" name="opt' . $indice . '" value="1"> SI<br>';
                echo '<input type="radio" class="resRadio" id="pruebaN" onclick="mostrarModal(' . $row['id'] . ', 2)" name="opt' . $indice . '" value="2"> NO';
                echo " </td>";
                echo "<td style=display:none class='editable' ></td>";
                echo "<td style=display:none id='vacio' ></td>";
                echo "</tr>";
                $indice++;
            };
        } else {
            //si no trae nada
            echo 1;
        }



        return $data;
    }



    public function traerRepuesta($documento, $fechaSistema, $val, $idE, $DATA2)
    {

        $db = $this->modeloP->conectar();

     

        // var_dump($DATA2);

        // Saco el id del paciente  para buscar el id_usu_paciente y registrar la valoracion

        $query =   sprintf(
            "SELECT  *  FROM paciente WHERE  numeroDocumento='%s' ",
            mysql_real_escape_string($documento)
        );

        $data = mysql_query($query, $db);

        if (!$data) {
            echo "Error de BD, no se pudo consultar la base de datos\n";
            echo "Error MySQL: " . mysql_error();
            exit;
        }

        $paciente = mysql_fetch_array($data);
        $idP = $paciente['id'];
        $estado=$paciente['estado'];


        // SELECT * FROM valoracion WHERE id_paciente=<id_paciente> ORDER BY fechaValora DES LIMIT 1


        if($estado =='Activo'){

        $query2 =   sprintf(
            "SELECT  *  FROM  valoracion WHERE  id_paciente='%s' ORDER BY idV DESC LIMIT 2 ",
            mysql_real_escape_string($idP)

        );

        $data2 = mysql_query($query2, $db);

        if (!$data2) {
            echo "Error de BD, no se pudo consultar la base de datos\n";
            echo "Error MySQL: " . mysql_error();
            exit;
        }


        $row = mysql_fetch_array($data2);
            $fecha = $row['fechaValora'];
            $valoracion1 = $row['idV'];

            // var_dump($row);
            // var_dump($valoracion1);
            // var_dump($fecha);
    


            $fech4=date("y-m-d",strtotime($fecha));
  
            // var_dump($fech4);


              $query4 =   sprintf(

                "SELECT p.tipo_id , t.nombre,t.id
            FROM preguntas AS p
            INNER JOIN respuesta AS r ON r.pregunta_id=p.id
            INNER JOIN tipovalora AS t ON t.id=p.tipo_id
            INNER JOIN valoracion AS v ON r.idV=v.idV
            WHERE r.idV ='%s'  ",
                mysql_real_escape_string($valoracion1)

            );

            $data4 = mysql_query($query4, $db);

            if (!$data4) {
                echo "Error de BD, no se pudo consultar la base de datos\n";
                echo "Error MySQL: " . mysql_error();
                exit;
            }

            $fila4 = mysql_fetch_assoc($data4);

            $tipo_idP = $fila4['tipo_id'];
            $nombre = $fila4['nombre'];

    

            // var_dump($nombre);
            // var_dump($tipo_idP);
           
    // 

      $t = strcmp($val, $tipo_idP); // da 0 cuando es igual y 1 cuando es diferente

       
 
          
    
        if ($t  == !0 ) {
 
            // var_dump('tal cual');

            $query1 = sprintf(
                "INSERT INTO valoracion(id_paciente,id_encargado,fechaValora) VALUES ('%s','%s','%s')",

                mysql_real_escape_string($idP),
                mysql_real_escape_string($idE),
                mysql_real_escape_string($fechaSistema)

            );


            $data1 = mysql_query($query1, $db);

            //recibo el último id
            $ultimo_id = mysql_insert_id($db);


            if (!$data1) {
                echo "Error de BD, no se pudo consultar la base de datos\n";
                echo "Error MySQL: " . mysql_error();
                exit;
            }



            for ($i = 0; $i < count($DATA2); $i++) {


                // var_dump($DATA2);

                $query3 = sprintf(
                    "INSERT INTO respuesta (pregunta_id ,respuesta,idV) VALUES ('%s','%s','%s')",

                    mysql_real_escape_string($DATA2[$i][0]),
                    mysql_real_escape_string($DATA2[$i][1]),
                    mysql_real_escape_string($ultimo_id)


                );


                $data3 = mysql_query($query3, $db);


                // Comprueba si fallo la consulta, si no hay errores continua
                if (!$data3) {
                    echo "Error de BD, no se pudo consultar la base de datos\n";
                    echo "Error MySQL: " . mysql_error();
                    exit;
                }
            }
            
            $result = '2';

        } else {
           

            $result= '0';
        }




        
    }
    // else{
        
        
        //     var_dump('no se pudo');
        // }
        
        
        return $result;


    
    }

    public function buscarRecomendacion($id_p)
    {
        $db = $this->modeloP->conectar();

        $query = sprintf(
            "SELECT  * FROM recomendacion WHERE id_pregunta= '%s' ",
            mysql_real_escape_string($id_p)
        );


        $data = mysql_query($query, $db);


        if (!$data) {
            echo "Error de BD, no se pudo consultar la base de datos\n";
            echo "Error MySQL: " . mysql_error();
            exit;
        }

        $json = [];


        while ($fila = mysql_fetch_assoc($data)) {

            $idR = $fila['idRe'];
            $des = $fila['descripcion'];

            $jsonTemp[] = array(

                'idRe' => $idR,
                'descripcion' => $des
            );

            array_push($json, $jsonTemp);
        }


        // el return no sirve para enviar json eso sirve para envio request


        $result = json_encode($json);  // sirve para pasar un $json a un string  , decode decodificar un String a json

        echo $result;
    }


    public function recomendacionID($DATA, $valor)
    {

        $db = $this->modeloP->conectar();

        // var_dump($DATA[1]);

        for ($i = 0; $i < count($DATA); $i++) {
            // var_dump($DATA[$i][0]);

            $query =   sprintf(
                "UPDATE respuesta SET id_recomendacion='%s' WHERE pregunta_id='%s' AND respuesta ='%s' ",

                mysql_real_escape_string($DATA[$i][0]),
                mysql_real_escape_string($DATA[$i][1]),
                mysql_real_escape_string($valor)


            );

            // var_dump($query);
            $data = mysql_query($query, $db);

            // // Comprueba si fallo la consulta, si no hay errores continua

            if (!$data) {
                echo "Error de BD, no se pudo consultar la base de datos\n";
                echo "Error MySQL: " . mysql_error();
                exit;
            }
        }
    }
}
