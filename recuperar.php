
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="vistas/vistas/img/favicon.ico">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Terapia Ocupacional</title>

        <!-- Bootstrap core CSS -->
        <link href="vistas/vistas/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template -->
        <link href="vistas/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

        <!-- Plugin CSS -->
        <link href="vistas/vistas/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

        <!-- Custom styles for this template -->
        <link href="vistas/vistas/css/freelancer.min.css" rel="stylesheet">

    </head>

    <body >
        <div class= "col-md-offset-4">
            <!-- Navigation -->


            <!-- Contact Section -->
            <section id="Registrar">
                <div class="container">
                    <h2 class="text-center text-uppercase text-secondary mb-0"> Verificar Usuario </h2>
                    <hr class="star-dark mb-5">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                            <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                            <form  id="formRecuperarC" name="formRecuperarC" action="javascript:validarContraseña()" method="post" >
                                <div class="control-group">
                                    <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                        <label class="text-muted">Usuario</label>
                                        <input class="form-control" id="usuario" name="usuario" type="text" placeholder="Usuario" required="required" data-validation-required-message="ingrese su nombre">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>

                               
                             
                                <br>

                                <div class="row" style="padding-top: 10px;">

                                    <div class="col-md-3">

                                        <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="index.html"> << Volver </a>
                                    </div>

                                    <div class="col-md-6" >

                                        <button type="submit" id="btnregUsuario" class="btn btn-block btn-primary btn-xl bg-primary">Validar</button>
                                    </div>

                                    <div class="col-md-3">

                                        <label id="camporesultado">
                                        </label>        

                                    </div> 
                                </div>
                            </form>


                       </div>
                    </div>
                </div>
            </section>

        </div>

        <!-- Footer -->
       
        <!--
                   <div class="copyright py-4 text-center text-white">
                       <div class="container">
                           <small>Copyright &copy; Maria Fernanda Palencia 2018</small>
                       </div>
                   </div>
               </div>
        -->
        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->



        <!-- Portfolio Modals -->

        <!-- Portfolio Modal 1 -->



        <!-- Bootstrap core JavaScript -->
        <script src="vistas/vistas/vendor/jquery/jquery.min.js"></script>
        <script src="vistas/vistas/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="vistas/vistas/vendor/jquery-easing/jquery.easing.min.js"></script>
        <script src="vistas/vistas/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

        <!-- Contact Form JavaScript -->
        <script src="vistas/vistas/js/jqBootstrapValidation.js"></script>
        <script src="vistas/vistas/js/contact_me.js"></script>

        <!-- Custom scripts for this template -->
        <!-- <script src="vistas/vistas/js/freelancer.min.js"></script> -->
        <script src="ajax/validarContraseña.js"></script>
            <script src="https://cdn.rawgit.com/alertifyjs/alertify.js/v1.0.10/dist/js/alertify.js"></script>
    </body>

</html>
