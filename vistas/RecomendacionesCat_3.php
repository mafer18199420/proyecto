<?php

session_start()


?>
<!DOCTYPE html>
<html lang="en">

<?php
include "plantilla/head.php"
?>

<body class="skin-blue fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/aside.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">RECOMENDACIONES DE LA VALORACION</h4>
                            <h6 class="card-subtitle"> </h6>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form class="input-form" action="javascript:buscarValoracion()" method="post" autocomplete="off">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-group mb-3">
                                                    <input class="form-control" id="documentoR" name="documentoR" type="number" required="required" data-validation-required-message="ingrese su documento">
                                                    <div class="input-group-append" id="divBoton" name="divBoton">
                                                        <button class="btn btn-info" type="submit" id="btnbuscarTerapia">Buscar...</button>
                                                    </div>
                                                </div>
                                                <div class="input-group row">
                                                    <label class="col-2 col-form-label">Valoracion</label>
                                                    <div class="col-10">
                                                        <select id="idValoracion" name="idValoracion" class="custom-select col-12">
                                                            <option value=""> Selecione una Valoracion</option>

                                                        </select>
                                                    </div>
                                                </div>



                                            </div>
                                            <br>
                                        </div>



                                    </form>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">RECOMENDACIONES</h4>
                            <h6 class="card-subtitle"></h6>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="bg-info">TIPO VALORACION</th>
                                            <th class="bg-info">CARACTERISTICAS</th>

                                            <th class="bg-info">RECOMENDACION</th>

                                        </tr>
                                    </thead>
                                    <tbody id="bodyT">






                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <?php
    include "plantilla/scriptfooter.php"
    ?>

    <script src="files/assets/node_modules/sweetalert/sweetalert.min.js"> </script>
    <script src="../ajax/recomendacionBus.js"></script>
</body>

</html>