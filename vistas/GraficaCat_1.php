<!DOCTYPE html>
<?php session_start() ?>
<html lang="en">

<?php
include "plantilla/head.php"
?>

<body class="skin-blue fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/aside.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">BUSCAR GRAFICAS</h4>
                            <h6 class="card-subtitle"> </h6>
                            <form id="formA" name="formConTerapia" action="javascript:buscarValoracion()" method="post" autocomplete="off">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <form class="input-form">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="input-group mb-3">
                                                        <input class="form-control" id="documentoR" name="documentoR" type="number" required="required" data-validation-required-message="ingrese su documento">
                                                        <div class="input-group-append" id="divBoton" name="divBoton">
                                                            <button class="btn btn-info" type="submit" id="btnbuscarTerapia">Buscar...</button>
                                                        </div>
                                                    </div>
                                                    <div class="input-group row">
                                                        <label class="col-2 col-form-label">Valoracion</label>
                                                        <div class="col-10">
                                                            <select id="idValoracion" name="idValoracion" class="custom-select col-12">
                                                                <option value=""> Selecione una Valoracion</option>

                                                            </select>
                                                        </div>
                                                    </div>



                                                </div>
                                                <br>
                                            </div>

                                        </form>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
 

                <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>


                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <?php
    include "plantilla/scriptfooter.php"
    ?>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="files/assets/node_modules/sweetalert/sweetalert.min.js"> </script>
    <script src="../ajax/graficaValoracion.js"></script>
</body>

</html>