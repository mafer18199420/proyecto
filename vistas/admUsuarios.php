<?php
include_once(__DIR__ . '/../controladores/ControladorUsuario.php');

session_start();
$controlador = new ControladorUsuario();
$data = $controlador->buscarUsuarios();
?>

<!DOCTYPE html>
<html lang="en">

<?php
include "plantilla/head.php"
?>

<body class="skin-blue fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/asideAdm.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->


                <br>
                <br>

                <div class="row">
                    <div class="col-lg-12">

                        <h2 class="page-header text-center">
                            Consultar Usuarios
                        </h1>

                    </div>
                </div>

                <br>
                <br>
                <form id="tableUsuarios" name="tableUsuarios">
                    <div class="row justify-content-center">

                        <div class="col-lg-10 col-md-offset-1">

                            <div class="table-responsive">
                                <table class="table table-striped table-sm">
                                    <thead>
                                        <tr>
                                            <th class="bg-info">Documento</th>
                                            <th class="bg-info">Nombres</th>
                                            <th class="bg-info">Apellidos</th>
                                            <th class="bg-info">Telefono</th>
                                            <th class="bg-info">Correo</th>
                                            <th class="bg-info">Fecha Nacimiento</th>
                                            <th class="bg-info">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="bodyD">

                                        <?php
                                        while ($fila = mysql_fetch_assoc($data)) {


                                            if ($m = $fila["tipoU"] == "3") {

                                                echo "<tr>";
                                                echo "<td>" . $fila["numeroDocumento"] . "</td>";
                                                echo "<td>" . $fila["nombres"] . "</td>";
                                                echo "<td>" . $fila["apellidos"] . "</td>";
                                                echo "<td>" . $fila["telefono"] . "</td>";
                                                echo "<td>" . $fila["email"] . "</td>";
                                                echo "<td>" . $fila["fechaNacimiento"] . "</td>";
                                                echo '<td><button class="btn btn-success" id="boton" onclick="activarUsuario(' . $fila["id"] . ')">Agregar</button></td>';
                                                echo "</tr>";
                                            }
                                        }


                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </form>

                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <?php
    include "plantilla/scriptfooter.php"
    ?>
    <!-- <script src="../ajax/consultarUsuarios.js"></script> -->
    <!-- <script src="../ajax/actualizarPaciente.js"></script> -->
    <script src="../ajax/validarUsuario.js"></script>

</body>

</html>