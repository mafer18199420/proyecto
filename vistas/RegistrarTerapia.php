<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">

<?php
include "plantilla/head.php"
?>

<body class="skin-blue fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/aside.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->
                <form action="">
                    <input type="hidden" id="idEncargado" value="<?php echo $_SESSION['id'] ?>">

                </form>
                <!-- .row -->
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title"> Registrar Intervencion</h4>
                            <h6 class="card-subtitle">  </h6>
                            <div class="row">
                                <div class=" col-sm-12">
                                    <div class="card">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center" id="contForPaciente" style="display:block">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header bg-info">
                            <h4 class="m-b-0 text-dark">Registra Intervencion del paciente</h4>
                            </div>
                            <div class="card-body">
                                <form id="formRegT" name="formRegT" action="javascript:registrarTerapia()" method="post" autocomplete="off">
                                    <div class="form-body">
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Numero Documento</label>
                                            <div class="col-xs-6 col-md-9">
                                                <input class="form-control" id="documento" name="documento" type="number" required="required" data-validation-required-message="ingrese su nombre">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3"> Nombre del Responsable</label>
                                            <div class="col-xs-6 col-md-9">
                                                <input class="form-control" id="responsable" name="responsable" type="text" placeholder="name" required="required" data-validation-required-message="Please enter your name">
                                                <small class="form-control-feedback"> </small> </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Cantidad de sesion</label>
                                            <div class="col-xs-6 col-md-9">
                                                <input name="cantidad" id="cantSesion" name="cantSesion" type="number" class="form-control">
                                                <small class="form-control-feedback"> </small> </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Fecha Inicio</label>
                                            <div class="col-xs-6 col-md-9 center">
                                                <input name="finicio" id="fechaInicio" name="fechaInicio" type="date" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Descripcion</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" rows="5" id="descripcion" name="descripcion"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row justify-content-center">
                                                    <div class="offset-sm-3 col-md-9">
                                                        <input hidden name="idE" id="idE">
                                                        <button class="btn btn-success " type="submit" id="btnBuscarRCat1" name="btnBuscarRCat1"> <i class="fa fa-check"></i>Enviar</button>
                                                        <button type="button" class="btn btn-inverse">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>



                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <?php
    include "plantilla/scriptfooter.php"
    ?>
    <script src="files/assets/node_modules/sweetalert/sweetalert.min.js"> </script>
    <script src="../ajax/registrarTerapia.js"></script>
</body>

</html>