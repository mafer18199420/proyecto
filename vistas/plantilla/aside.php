<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="user-pro"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><img src="files/assets/images/users/rt.jpg" alt="user-img" class="img-circle"><span class="hide-menu"> <?php echo $_SESSION['nombres']?>  <?php echo $_SESSION['apellidos']?></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="perfil.php"><i class="fa fa-fw fa-user"></i> Perfil </a></li>           
                        <li><a href="../index.php" class="dropdown-item"><i class="fa fa-power-off"></i> Cerrar Sesion</a></li>
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-fw fa-user"></i><span class="hide-menu">Datos del Usuario </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="registrarPaciente.php"><i class="fas fa-check"></i> Registrar</a></li>
                        <li><a href="consultarPaciente.php"><i class="fas fa-check"></i> Consultar </a></li>
                        <li><a href="actualizarPaciente.php"><i class="fas fa-check"></i>Actualizar </a></li>
                        <li><a href="actEstadoPaciente.php"> <i class="fas fa-check"></i> Actualizar estado </a></li>
                    </ul>
                </li>


                <li> 
                    <a class="waves-effect waves-dark" href="RegistrarInforme.php" aria-expanded="false"><i class="fas fa-edit"></i><span class="hide-menu">Registrar Valoracion</span></a>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-list-alt"></i><span class="hide-menu">Objetivos de Intervencion </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="RecomendacionesCat_3.php"><i class="fas fa-check"></i> Vista General recomendaciones </a></li>
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-chart-pie"></i><span class="hide-menu">Resultados Individuales</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li> <a href="GraficaCat_1.php"><i class="fas fa-check"></i> Resultados graficas Individuales </a></li>
                      
                    </ul>
                </li>

                
                <li> 
                    <a class="waves-effect waves-dark" href="ConsultaGeneralM.php" aria-expanded="false"><i class="far fa-chart-bar"></i><span class="hide-menu">Resultados Generales</span></a>
                </li>
               
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-marker"></i><span class="hide-menu">Evolucion del Usuario</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="RegistrarTerapia.php"><i class="fas fa-check"></i> Registrar Intervencion </a></li>
                        <li><a href="RegistrarSesion.php"> <i class="fas fa-check"></i> Registrar Evolucion</a></li>
                        <li><a href="ConsultarAvanceTerapia.php"><i class="fas fa-check"></i> Consultar Evolucion</a></li>
                    </ul>
                </li>

                <li> 
                    <a class="waves-effect waves-dark" href="consultarHistoria.php" aria-expanded="false"><i class="far fa-clipboard"></i><span class="hide-menu">Consultar Historia Clinica</span></a>
                </li>



            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>