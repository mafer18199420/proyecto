<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="user-pro"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><img src="files/assets/images/users/rt.jpg" alt="user-img" class="img-circle"><span class="hide-menu"> <?php echo $_SESSION['nombres']?>  <?php echo $_SESSION['apellidos']?></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="perfilAdmin.php"><i class="fa fa-fw fa-user"></i> Perfil </a></li>           
                        <li><a href="../index.php" class="dropdown-item"><i class="fa fa-power-off"></i> Cerrar Sesion</a></li>
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-fw fa-user"></i><span class="hide-menu">Usuario </span></a>
                    <ul aria-expanded="false" class="collapse">
                       
                        <li><a href="admUsuarios.php"><i class="fas fa-check"></i>Consultar Usuarios</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>