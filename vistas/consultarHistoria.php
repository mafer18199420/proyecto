<!DOCTYPE html>
<?php session_start() ?>
<html lang="es">

<?php
include "plantilla/head.php"
?>

<body class="skin-blue fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/aside.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">CONSULTAR HISTORIA</h4>
                            <h6 class="card-subtitle"> </h6>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form class="input-form" action="javascript:consultarHistorial()" method="post" autocomplete="off">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-group mb-3">
                                                    <input class="form-control" id="documentoHistorial" name="documentoR" type="number" required="required" data-validation-required-message="ingrese su documento">
                                                    <div class="input-group-append" id="divBoton" name="divBoton">
                                                        <button class="btn btn-info" type="submit" id="btnbuscarTerapia">Buscar...</button>
                                                    </div>
                                                </div>

                                            </div>
                                            <br>
                                        </div>



                                    </form>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <br>



                <form id="tableHistorial" name="tableHistorial">
                    <div class="row">

                        <div class="col-lg-10 col-md-offset-1">

                            <div class="table-responsive">
                                <table id="tablehistorial" name="table" class="table table-bordered table-hover table-striped bg-info">
                                    <thead>
                                        <tr>
                                            <th style="display:none" class="bg-info">id</th>
                                            <th id="motivo" class="bg-info">Motivo</th>
                                            <th class="bg-info">Nombres</th>
                                            <th class="bg-info">Apellidos</th>
                                            <th id="fecha" class="bg-info">Fecha Registro</th>
                                            <th class=" text-nowrap bg-info">Observacion</th>

                                        </tr>
                                    </thead>
                                    <tbody id="bodyH">


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </form>





                <div id="myModalVerSesion" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4  class="modal-title text-center"> Informacion Intervencion </h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div style="text-align: center" class="modal-body">


                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Responsable</th>
                                                <th>Descripcion</th>
                                                <th>Fecha Registro </th>

                                            </tr>
                                        </thead>
                                        <tbody id="bodyS">


                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="modal-footer">

                                <div class="form-group">
                                    <div class="col-md-3 col-md-offset-4">

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>


                <div id="myModalVerTerapia" name="myModalVerTerapia" class="modal fade modal-wide" role="dialog">
                    <div class="modal-dialog modal-lg ">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4  class="modal-title text-center"> Informacion Evolucion </h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div style="text-align: center" class="modal-body">
                            

                                <div style="text-align: center" class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Responsable</th>
                                                <th>Descripcion</th>
                                                <th>Cantidad de sesion</th>
                                                <th>Anvance</th>
                                                <th>Estado</th>
                                                <th>Fecha Inicio</th>

                                            </tr>
                                        </thead>
                                        <tbody id="bodyTe">


                                        </tbody>
                                    </table>
                                </div>


                            </div>
                            <div class="modal-footer">

                                <div class="form-group">
                                    <div class="col-md-3 col-md-offset-4">

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>


                <div id="myModalVerInforme" name="myModalVerInforme" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg ">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header text-center">
                                <h4 style="text-align: center" class="modal-title"> Informacion Valoracion </h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div style="text-align: center" class="modal-body">

                                <div style="text-align: center" class="table-responsive">
                                    <table class="table  table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>CARACTERISTICA</th>
                                                <th>RECOMENDACION</th>

                                            </tr>
                                        </thead>
                                        <tbody id="bodyT">




                                        </tbody>
                                    </table>

                                </div>


                            </div>
                            <div class="modal-footer">

                                <div class="form-group">
                                    <div class="col-md-3 col-md-offset-4">

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>


                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <?php
    include "plantilla/scriptfooter.php"
    ?>
    <script src="files/assets/node_modules/sweetalert/sweetalert.min.js"> </script>
    <!-- <script src="ajax/registrarPaciente.js"> </script> -->
    <!-- <script src="../ajax/actualizarPaciente.js"></script> -->
    <script src="../ajax/consultarHistorial.js"></script>
</body>

</html>