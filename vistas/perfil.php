<!DOCTYPE html>
<?php session_start() ?>
<html lang="es">

<?php
include "plantilla/head.php"
?>

<body class="skin-blue fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/aside.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->

                    <br>
                <div class="row" id="contForPaciente" style="display:block">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                 
                                <h4 class="card-title">PERFIL DEL PROFESIONAL</h4>
                            </div>
                            <div class="card-body">
                                <form id="formActualizarUsuario" style="display:block" action="" method="post">
                                    <div class="form-body">
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Nombres</label>
                                            <div class="col-xs-6 col-md-9"> 
                                            <input type="text" placeholder="id" class="form-control " id="ide" name="ide" style="display:none" value="<?php echo $_SESSION['id'] ?>"> 
                                                <input type="text" placeholder="nombres" class="form-control " id="nombres" name="nombres" value="<?php echo $_SESSION['nombres'] ?>">
                                                <small class="form-control-feedback">  </small> </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Apellidos</label>
                                            <div class="col-xs-6 col-md-9">
                                                <input type="text" placeholder="apellidos" class="form-control" id="apellidos" name="apellidos" value="<?php echo $_SESSION['apellidos'] ?>">
                                                <small class="form-control-feedback"> </small> </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Direccion</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $_SESSION['direccion'] ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Numero de telefono</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $_SESSION['telefono'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Fecha de cumple años</label>
                                            <div class="col-xs-6 col-md-9 center">
                                                <input type="date" class="form-control" placeholder="dd/mm/yyyy" id="fnacimiento" name="fnacimiento" value="<?php echo $_SESSION['fechaN'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">email</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="email" name="email" value="<?php echo $_SESSION['email'] ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Contraseña</label>
                                            <div class="col-md-9">
                                                <input type="password" class="form-control" id="contra" name="contraseña" value="<?php echo $_SESSION['contra'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Confirmar Contraseña</label>
                                            <div class="col-md-9">
                                                <input type="password" class="form-control" id="conContra" name="Contraseña" value="<?php echo $_SESSION['contra'] ?>">
                                            </div>
                                        </div>



                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="offset-sm-3 col-md-9">
                                                        <button type="button" id="actualizarU" class="btn btn-success"> <i class="fa fa-check"></i>Actualizar</button>
                                                        <button type="button" onclick="resetFormulario()" class="btn btn-inverse">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>








                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <?php
    include "plantilla/scriptfooter.php"
    ?>
    <script src="files/assets/node_modules/sweetalert/sweetalert.min.js"> </script>
    <script src="../ajax/actualizarDatos.js"></script>
</body>

</html>