<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
include "plantilla/head.php"
?>

<body class="skin-blue fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/aside.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">Consultar Evolucion</h4>
                            <h6 class="card-subtitle"> </h6>
                            <form id="formA" name="formConTerapia" action="javascript:buscarTerapias()" method="post" autocomplete="off">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <form class="input-form">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="input-group mb-3">
                                                        <input class="form-control" id="documentoT" name="documentoT" type="number" required="required" data-validation-required-message="ingrese su documento">
                                                        <div class="input-group-append" id="divBoton" name="divBoton">
                                                            <button class="btn btn-info" type="submit" id="btnbuscarTerapia">Buscar...</button>
                                                        </div>
                                                    </div>
                                                    <div class="input-group row">
                                                        <label class="col-2 col-form-label">Terapias</label>
                                                        <div class="col-10">
                                                        <select id="idTerapia" name="idTerapia" class="custom-select col-12">
                                                            <option value=""> Selecione Terapia</option>

                                                        </select>
                                                        </div>
                                                    </div>



                                                </div>
                                                <br>
                                            </div>



                                        </form>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Info Table</h4>
                            
                                <div class="table-responsive">
                                    <table id="tablaTerapia" class="table table-bordered table-hover table-striped bg-info">
                                        <thead>
                                            <tr>
                                                <th>Nombre Terapeuta</th>
                                                <th>Descripcion Evolucion</th>
                                                <th>Cantidad de Intervenciones</th>
                                                <th>Avancess</th>
                                                <th>Estado</th>
                                                <th>Fecha Inicio</th>
                                            </tr>
                                        </thead>
                                        <tbody  id = "bodyT">
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>

              
               


                
               
                
                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <?php
    include "plantilla/scriptfooter.php"
    ?>

    <script src="files/assets/node_modules/sweetalert/sweetalert.min.js"> </script>
    <script src="../ajax/buscarTerapias.js"></script>
    <!-- <script src="../ajax/MostrarTerapia.js"></script> -->

</body>

</html>