<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="perfil.php"></a>
        <img class="img-fluid" src="vistas/img/logo2.png" alt="" >
    </div>



    
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['nombres']?>  <?php echo $_SESSION['apellidos']?>
                <b class="caret"></b><a>
                <ul class="dropdown-menu">
                <li>
                    <a href="perfil.php"><i class="fa fa-fw fa-user"></i> Perfil </a>
                </li>
                <li>
                    <a href="actualizarDatos.php"><i class="fa fa-fw fa-gear"></i> Actualizar Datos</a>
                </li>
                <li class="divider"></li>
                <li>

                    <a  href="../index.php"><i class="fa fa-fw fa-power-off"></i> Salir </a>

                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse ">
        <ul class="nav navbar-nav side-nav">
            <li>
                <a  href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-edit "></i>  Usuario </a>
                <ul id="demo"  class="collapse" >
                    <li>
                        <a href="registrarPaciente.php">Registrar  Usuario</a>

                    </li>

                    <li>
                        <a href="consultarPaciente.php">Consultar  Usuarios</a>
                    </li>
                    <li>
                        <a href="actualizarPaciente.php">Actualizar  Usuario</a>
                    </li>
                    <li>
                        <a href="actEstadoPaciente.php">Actualizar estado del  Usuario</a>
                    </li>
                </ul>
            </li>


            <li >
                <a href="RegistrarInformeCat_1.php" data-toggle="collapse" data-target="#demo0"><i class="fa fa-fw fa-edit"> </i> Registrar Valoracion </a>


            </li>





            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-table"></i> Recomendaciones</a>

                <ul id="demo2"  class="collapse">
                    <li>
                        <a href="RecomendacionesCat_1.php"> Motoras Y Praxis </a>
                    </li>
                    <li>
                        <a href="RecomendacionesCat_2.php">Sensoriales Y Perceptuales </a>
                    </li>

                    <li>
                        <a href="RecomendacionesCat_3.php">Cognitivas </a>
                    </li>
                    <li>
                        <a href="RecomendacionesCat_4.php">Comunicación y Sociales</a>
                    </li>
                    <li>
                        <a href="RecomendacionesCat_5.php">Regulación Emocional</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-stats"></i> Resultados Individuales </a>
                <ul id="demo3"  class="collapse" >
                    <li>
                        <a href="GraficaCat_1.php"> Motoras Y Praxis </a>
                    </li>
                    <li>
                        <a href="GraficaCat_2.php">Sensoriales Y Perceptuales </a>
                    </li>

                    <li>
                        <a href="GraficaCat_3.php">Cognitivas </a>
                    </li>
                    <li>
                        <a href="GraficaCat_4.php">Comunicación y Sociales</a>
                    </li>
                    <li>
                        <a href="GraficaCat_5.php">Regulación Emocional</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo6"><i class="glyphicon glyphicon-stats"> </i> Resultados Generales</a>

                <ul id="demo6"   class="collapse">
                    <li>
                        <a href="ConsultaGeneralM.php"> Resultados generales</a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-edit"></i> Evolucion del Paciente </a>

                <ul id="demo4"  class="collapse" >
                    <li>
                        <a href="RegistrarTerapia.php">Registrar Evolucion </a>
                    </li>
                    <li>
                        <a href="RegistrarSesion.php">Registrar Intervencion</a>

                    </li>

                    <li>
                        <a href="ConsultarAvanceTerapia.php">Consultar Evolucion</a>
                    </li>

                </ul>
            </li>



            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-table"></i> Resumen de Historia </a>

                <ul id="demo5"  class="collapse">
                    <li>
                        <a href="consultarHistoria.php"> Consultar Resumen de Historia</a>
                    </li>

                </ul>
            </li>






        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>
