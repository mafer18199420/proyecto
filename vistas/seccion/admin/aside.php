
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="perfil.php"></a>
        <img class="img-fluid" src="vistas/img/logo2.png" alt="" >
    </div>



    
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['nombres']?>  <?php echo $_SESSION['apellidos']?>
                <b class="caret"></b><a>
                <ul class="dropdown-menu">
                <li>
                    <a href="perfilAdmin.php"><i class="fa fa-fw fa-user"></i> Perfil </a>
                </li>
                <li>
                    <a href="actualizarDatosAdmin.php"><i class="fa fa-fw fa-gear"></i> Actualizar Datos</a>
                </li>
                <li class="divider"></li>
                <li>

                    <a  href="../cerrar.php"  ><i class="fa fa-fw fa-power-off"></i> Salir </a>

                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse ">
        <ul class="nav navbar-nav side-nav">
            


            <li >
                <a href="adminnistracionUsuarios.php" data-toggle="collapse" data-target="#demo0"><i class="fa fa-fw fa-edit"> </i> Admin. Usuarios </a>


            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>
