<?php
include_once(__DIR__ . '/../controladores/ControladorPaciente.php');

session_start();

$idEncar = $_SESSION["id"];


$controlador = new ControladorPaciente();
$data = $controlador->listarPacientes($idEncar);
?>

<!DOCTYPE html>
<html lang="es">
<?php
include "plantilla/head.php"
?>


<body class="skin-blue fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/aside.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->


                <div class="row ">

                </div>
                <br>
                <br>

                <div class="row ">

                    <div class="col-lg-12">

                        <h2 class="page-header text-center">
                            Consultar Usuarios
                            </h1>

                    </div>
                </div>
                <br>
                <br>

                <form id="tablePacientes" name="tablePacientes">
                    <div class="row justify-content-center">

                        <div class="col-lg-10 col-md-offset-1">

                            <div class="table-responsive">
                                <table id="table" name="table" class="table table-bordered table-hover table-striped ">
                                    <thead>
                                        <tr>
                                            <th class="bg-info">Nombres</th>
                                            <th class="bg-info">Apellidos</th>
                                            <th class="bg-info">Documento</th>
                                            <th class="bg-info">Edad</th>
                                            <th class="bg-info">Telefonoo</th>
                                            <th class="bg-info">Direccion</th>
                                            <th class="bg-info">Fecha Naciemiento</th>
                                            <th class="bg-info">Fecha de Ingreso</th>
                                            <th class="bg-info">Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        while ($fila = mysql_fetch_assoc($data)) {
                                            echo "<tr>";
                                            echo "<td>" . $fila["nombres"] . "</td>";
                                            echo "<td>" . $fila["apellidos"] . "</td>";
                                            echo "<td>" . $fila["numeroDocumento"] . "</td>";
                                            echo "<td>" . $fila["edad"] . "</td>";
                                            echo "<td>" . $fila["telefono"] . "</td>";
                                            echo "<td>" . $fila["direccion"] . "</td>";
                                            echo "<td>" . $fila["fechaNacimiento"] . "</td>";
                                            echo "<td>" . $fila["fechaRegistro"] . "</td>";
                                            echo "<td>" . $fila["estado"] . "</td>";
                                            echo "</tr>";
                                        }

                                        ?>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </form>




                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <?php
    include "plantilla/scriptfooter.php"
    ?>

    <script src="../ajax/consultarPaciente.js"></script>
</body>

</html>