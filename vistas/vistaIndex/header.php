<body id="page-top">
    <div class="col-md-offset-4">
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg bg-primary fixed-top text-uppercase" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top">

                    <img class="img-fluid" src="vistas/vistas/img/logo.png" alt="">


                </a>

                <button class="navbar-toggler navbar-toggler-right text-uppercase text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mx-0 mx-lg-1">
                            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger  portfolio-item d-block mx-auto bg-primary" href="#modal-1">Inciar Sesion</a>
                        </li>

                        <li class="nav-item mx-0 mx-lg-1">
                            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger bg-primary" href="#Acerca"> Acerca De </a>
                        </li>
                        <li class="nav-item mx-0 mx-lg-1">
                            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger bg-primary" href="#portfolio"> Sabias </a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>

        <!-- Header -->
        <header class="masthead bg-secundary text-white text-center">
            <div class="container">
                <img class="img-fluid mb-5 d-block mx-auto" src="vistas/vistas/img/logo3.png" alt="">
                <h1 class="text-uppercase mb-0 bg-secundary"> TOAMI</h1>
                <br>
                <h3 class="text-uppercase mb-0 bg-secundary">GUIA DE TERAPIA OCUPACIONAL PARA LA INTERVENCIÓN DE ADULTOS MAYORES INSTITUCIONALIZADOS</h2>
             <br>
                <h3 class="font-weight-light mb-0">TERAPIA OCUPACIONAL</h2>
                <h3 class="font-weight-light mb-0">Universidad De Santander</h2>

            </div>
        </header>