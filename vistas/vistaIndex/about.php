  <!-- About Section -->
  <section class="bg-primary text-white mb-0" id="Acerca">
      <div class="container">
          <h3 class="text-center text-uppercase text-white"> Acerca De </h2>
          <h4 class="text-center text-uppercase text-white"> Proyecto de investigación Interdisciplinario </h4>
          <!-- <hr class="star-light mb-5"> -->
          <br> 
          
          <div class="row">
              <div class="col-lg-4 ml-auto">
                  <p class="lead">Terapia Ocupacional</p>
                  <p> T.O Zaida Milena Vera Serrano <br> T.O Alvaro Enrique Pérez Valencia <br>Terapeutas Ocupacionales en formacion <br> T.O Daisy Johanna Contreras Paredes <br>Terapeuta Ocupacional <p>

              </div>
              <br>
              <div class="col-lg-4 mr-auto">
                  <p class="lead">Ingenieria de Sistemas</p>
                  <p>Ing. Maria Fernanda Palencia Cáceres <br> Ingeniera en Formación <br>Ing. Maria del Pilar Rojas Puentes</p>
              </div>
          </div>
          <br>
          <p class="text-center text-uppercase text-white justify-content-center h5"> Todos los derechos reservados  <br> Udes</p>
         
      </div>
  </section>