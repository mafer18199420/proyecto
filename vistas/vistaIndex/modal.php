 <!-- Portfolio Modal 1 -->
 <div class="portfolio-modal mfp-hide" id="modal-1">
            <div class="portfolio-modal-dialog bg-white">
                <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
                    <i class="fa fa-3x fa-times"></i>
                </a>
                <div class="container text-center">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-secondary text-uppercase mb-0"> Iniciar Sesion</h2>
                            <hr class="star-dark mb-5">

                            <div class="modal-body">
                                <div class="container">


                                    <!-- Start Sign In Form -->
                                    <form id="formularioLogin" class="formularioLogin" action="javascript:iniciarSesion()" autocomplete="off">
                                        <div class="form-group  ">
                                            <label for="usu">Usuario</label>
                                            <input class="form-control" name="usu" id="usu" type="text" data-validation-required-message="Please enter your name.">
                                            <p class="help-block text-danger"></p>
                                        </div>
                                        <div class="form-group  ">
                                            <label for="contra">Contraseña</label>
                                            <input class="form-control" name="contra" id="contra" type="password" data-validation-required-message="Please enter your name.">
                                            <p class="help-block text-danger"></p>
                                        </div>


                                        <br>
                                        <div class="row">
                                            <div class="col-md-3">

                                                <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="index.php">
                                                    << Volver </a> </div> <div class="col-md-6">

                                                        <button type="submit" class="btn btn-primary btn-xl bg-primary" id="sendMessageButton"> Iniciar Sesion </button>



                                            </div>
                                            <div class="col-md-3">
                                                <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="recuperar.php"> ¿Olvidaste Tu Contraseña? </a>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="contra" id="mensaje"></label>
                                            </div>
                                    </form>

                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="contraIncorrecta" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">

                    </div>
                    <div class="modal-body center">
                        <h1>DATOS INCORRECTOS</h1>
                    </div>
                    <div class="modal-footer">
                        <a href="#" data-dismiss="modal" class="btn btn-danger">Cerrar</a>
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" id="usuarioNoAutorizado" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">

                    </div>
                    <div class="modal-body center">
                        <h1>USUARIO NO AUTORIZADO</h1>
                    </div>
                    <div class="modal-footer">
                        <a href="#" data-dismiss="modal" class="btn btn-danger">Cerrar</a>
                    </div>
                </div>
            </div>
        </div>
