  <!-- Portfolio Grid Section -->
  <section class="portfolio py-4" id="portfolio">
      <div class="container">
          <h3 class="text-center text-uppercase bg-secundary mb-0"> Sabias ¿Que?</h2>
          <hr class="star-dark mb-5">
          <div class="row">
              <div class="col-md-12 col-lg-12 center-block">

                  <p ALIGN="justify" class="font-weight-normal mb-0" >
                      La presente Guía de Terapia Ocupacional para la intervención de personas mayores institucionalizadas (TOAMI), surge del proyecto de carácter interdisciplinar que lleva por nombre
                      “Formulación de guía tecnológica de destrezas de ejecución ocupacional relacionadas con el proceso de envejecimiento de las personas mayores institucionalizadas en el hogar de Nazaret
                      Cúcuta, 2017". La guía tiene como propósito contribuir en la evaluación, registro y diseño de estrategias prácticas para los Terapeutas Ocupacionales con un abordaje integral a la
                      población geriátrica de las destrezas de ejecución ocupacional,las cuales determinan el nivel de funcionalidad del individuo en las diferentes áreas de desempeño; igualmente genera un
                      aporte importante para la profesión y la praxis profesional del Terapeuta Ocupacional en el área geriátrica, teniendo en cuenta que, la población objeto de estudio utilizada como base
                      para la consolidación de información y elaboración de esta, presenta como característica principal, la ausencia de diagnósticos clínicos asociados que pudieran alterar el proceso de
                      envejecimiento, por ende, la guía puede ser utilizada con la población mayor que requiera de la valoración e intervención desde Terapia Ocupacional dentro hogares o residencias geriátricas
                      y de continuarse el proceso investigativo con personas mayores no institucionalizadas.De igual manera, el uso de la Guía de Terapia Ocupacional para la intervención de personas mayores
                      institucionalizadas (TOAMI) podría proyectarse hacia los contextos regional, nacional e internacional según se requiera, considerando que, los objetivos de intervención planteados presentan
                      referencias bibliográficas propias de Terapia Ocupacional que son manejadas por los diferentes territorios en la praxis profesional.
                  </p>



              </div>

          </div>
      </div>
  </section>

  <section class="proyectos py-4">
    <div class="container">
      <div class="row">

        <article class="col-12 col-md-6 mb-3 col-lg-3 mb-lg-0">
          <div class="card">
            <img class="card-img-top h-100" src="vistas/vistas/img/portfolio/galeria1.png" alt="proyecto 1">
       

          </div>

        </article>
        <article class="col-12 col-md-6 mb-3 col-lg-3 mb-lg-0">
          <div class="card">
            <img class="card-img-top h-100" src="vistas/vistas/img/portfolio/galeria2.png" alt="proyecto 1">
          </div>

        </article>

        <article class="col-12 col-md-6 mb-3 col-lg-3 mb-lg-0">
          <div class="card">
            <img class="card-img-top h-100" src="vistas/vistas/img/portfolio/galeria3.png" alt="proyecto 1">
           

          </div>

        </article>
        <article class="col-12 col-md-6  mb-3 col-lg-3 mb-lg-0">
          <div class="card">
            <img class="card-img-top h-100" src="vistas/vistas/img/portfolio/galeria4.png" alt="proyecto 1">
         

          </div>

        </article>


      </div>
    </div>
  </section>



