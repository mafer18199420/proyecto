<section id="Registrar">
            <div class="container">
            <h1 class="text-uppercase text-center mb-0 bg-secundary"> Registarse</h1>
                <hr class="star-dark mb-4">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                        <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                        <form id="formRegUsuarios" name="formRegUsuarios" action="javascript:registrarUsuario()" method="post">
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label class="text-muted">Nombres</label>
                                    <input class="form-control" id="nombres" name="nombres" type="text" placeholder="Nombres" required="required" data-validation-required-message="ingrese su nombre">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label class="text-muted">Apellidos</label>
                                    <input class="form-control" id="apellidos" name="apellidos" type="text" placeholder="Apellidos" required="required" data-validation-required-message="ingrese su nombre">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label class="text-muted">Numero Documento</label>
                                    <input class="form-control" id="documento" name="documento" type="number" placeholder="Numero Documento" required="required" data-validation-required-message="ingrese su nombre">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label class="text-muted">Numero Telefono</label>
                                    <input class="form-control" id="telefono" name="telefono" type="number" placeholder="Telefono" required="required" data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label class="text-muted">Direccion</label>
                                    <input class="form-control" id="direccion" name="direccion" type="text" placeholder="Direccion" required="required" data-validation-required-message="ingrese su nombre">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label class="text-muted">Fecha Nacimiento</label>
                                    <input name="fnacimiento" id="fnacimiento" name="fnacimiento" type="date" class="form-control">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>




                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label class="text-muted">Email</label>
                                    <input class="form-control" id="email" type="email" name="email" placeholder="Email Address" required="required" data-validation-required-message="ingrese su email">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>

                            <br>

                            <div class="row" style="padding-top: 10px;">
                                <div class="col-md-offset-4 ">

                                    <button type="submit" id="btnregUsuario" class="btn btn-block btn-primary btn-xl bg-primary">Registrar</button>
                                </div>
                            </div>
                        </form>

                        <center>
                            <label id="camporesultado">
                            </label>
                        </center>
                    </div>
                </div>
            </div>
        </section>