<?php
include_once(__DIR__ . '/../controladores/ControladorTipoValoracion.php');

session_start();

$idEncar = $_SESSION["id"];


$controlador = new ControladorTipoValoracion();
$data = $controlador->tiposValoracion();
?>

<!DOCTYPE html>
<html lang="en">

<?php
include "plantilla/head.php"
?>

<body class="skin-blue fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/aside.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->

                <form action="">
                    <input type="hidden" id="idEncargado" value="<?php echo $_SESSION['id'] ?>">

                </form>
                <!-- .row -->
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">Buscar Usuario</h4>
                            <h6 class="card-subtitle"> Pagina para buscar Usuario y registrar valoraciones </h6>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form class="input-form">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-group mb-3">
                                                    <input type="text" id="registrarPacienteDocumento" class="form-control" placeholder="Buscar Documento" aria-label="" aria-describedby="basic-addon1">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-info" type="button" id="registrarPacienteBuscar">Buscar...</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>

                                        <!-- form-group -->
                                    </form>
                                </div>

                            </div>
                            <div class="row">
                                <div class=" col-sm-12">
                                    <div class="card">
                                        <div class="card-body" id="dataConsulta">

                                            <!-- info fconsulta -->


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <select class="custom-select col-12" id="tipoValoracion">
                                        <option value="">Tipo de Valoración</option>
                                        <?php
                                        while ($row2 = mysql_fetch_array($data)) {

                                            echo  '<option value="' . $row2['id'] . '">' . $row2['nombre'] . '</option>';
                                        };
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <!-- <button id="guarRespuesta">bbbbbb</button> -->
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Valoracion</h4>
                            <h6 class="card-subtitle"></h6>
                            <div class="table-responsive">
                                <table class="table table-striped" id="mitabla">
                                    <thead>
                                        <tr>
                                            <th style="display:none">id</th>
                                            <th>Preguntas</th>

                                            <th class="text-nowrap">Respuestas</th>
                                            <th style="display:none" class="editable">idres</th>
                                            <th style="display:none" id="vacio"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="preguntas">






                                    </tbody>
                                </table>

                            </div>
                            <br>
                            <div class="input-group-append  justify-content-center">
                                <button id="guarRespuesta" type="button" class="btn waves-effect waves-light btn-success">Registrar </button>

                            </div>
                        </div>
                    </div>
                </div>



                <!-- Modal -->
                <div class="modal fade" id="pruebaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Objetivos de intervención</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <input type="hidden" value="" id="idPregunta">
                            <div class="modal-body" id="idModal">


                            </div>
                         
                        </div>
                    </div>
                </div>




                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <?php
    include "plantilla/scriptfooter.php"
    ?>
    <script src="files/assets/node_modules/sweetalert/sweetalert.min.js"> </script>
    <script src="https://cdn.rawgit.com/alertifyjs/alertify.js/v1.0.10/dist/js/alertify.js"></script>
    <script src="../ajax/registrarValoracionPaciente.js"></script>
</body>

</html>