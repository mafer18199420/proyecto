
<!DOCTYPE html>
<html lang="es">
<?php include "seccion/adminT/head.php" ?>  

    <body>
        <div id="wrapper">

            <!-- Navigation -->
    <?php include "seccion/adminT/aside.php" ?>  

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-6 col-md-offset-3 ">
                            <h1 class="page-header text-center">
                                Actualizar Datos
                            </h1>

                        </div>
                    </div>



               
                        <div class="col-md-6  col-md-offset-3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title text-center">Datos Personales</h3>
                                </div>
                                <div class="panel-body">
                                    
                                    <form id="actualizarDatos" action="javascript:actualizarUsuario()" method="Post" autocomplete="off">
                                        <input  hidden name="idE" id="idE" value="<%= session.getAttribute("id")%>" >
                                    <div class="row">
                                    <div class="col-lg-6"><span class="text">Nombres: </span></div>
                                    <div class="col-lg-6"><input name="nombres" id="nombres" type="text"> </div>
                                    </div>
                                   
                                    <br>
                                    
                                    <div class="row">
                                    <div class="col-lg-6"><span class="text">Apellidos: </span></div>
                                    <div class="col-lg-6"><input name="apellidos" id="apellidos" type="text" > </div>
                                    </div>
                                    
                                     <br>
                                    
                                    <div class="row">
                                    <div class="col-lg-6"><span class="text">Direccion: </span></div>
                                    <div class="col-lg-6"><input name="direccion" id="direccion" type="text" > </div>
                                    </div>
                                     
                                      <br>
                                    
                                    <div class="row">
                                    <div class="col-lg-6"><span class="text">Telefono: </span></div>
                                    <div class="col-lg-6"><input name="telefono" id="telefono" type="text" > </div>
                                    </div>
                                       
                              
                                          <br>
                                    
                                     <div class="row">
                                    <div class="col-lg-6"><span class="text">Contraseña: </span></div>
                                    <div class="col-lg-6"><input name="contraseña" id="contraseña" type="text"> </div>
                                    </div>
                                    
                                    <br>
                                     <div class="row">
                                    <div class="col-lg-6"><span class="text">Confirmar Contraseña </span></div>
                                    <div class="col-lg-6"><input name="contraseñaC" id="contraseñaC" type="password" value=""
                                    class="form-control"> </div>
                                    </div>
                                    <br>
                                    
                                    
                                    <div class="row" style="padding-top: 10px;">
                                    <div class="col-lg-10 col-md-offset-5 ">
                                        <div class="control-group">
                                            <div class="col-md-4">                        
                                                <button type="submit" id="btnregPaciente" class="btn btn-primary btn-lg ">Actualizar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    </form>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        

                        <!-- /.row -->


                        <!-- /.row -->



                    </div>
                </div>

                <!-- /#page-wrapper -->

            </div>
            <?php include "seccion/adminT/script.php" ?>  

    </body>


</html>
