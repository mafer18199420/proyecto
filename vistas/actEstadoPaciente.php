<!DOCTYPE html>
<?php session_start() ?>
<html lang="es">
<?php
include "plantilla/head.php"
?>

<body class="skin-blue fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/aside.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">Actualizar Estado del Usuario</h4>
                            <h6 class="card-subtitle"> </h6>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form class="input-form" id="formA" name="ActualizarEstado" action="javascript:estadoPaciente()" method="post" autocomplete="off">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-group mb-3">
                                                    <input type="text" id="documentoA" class="form-control" placeholder="Buscar Documento" aria-label="" aria-describedby="basic-addon1">

                                                </div>

                                                <div class="alert alert-danger" id="error" style="display:none">El usuario no se encuentra registrado en el sistema. </div>

                                                <div class="input-group row">
                                                    <label class="col-2 col-form-label">Terapias</label>
                                                    <div class="col-10">
                                                        <select id="estado" name="estado" class="custom-select col-12">
                                                            <option value=""> Seleccion</option>
                                                            <option value="Activo"> Con Atencion </option>
                                                            <option value="Inactivo"> Sin Atencion </option>

                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="row" style="padding-top: 10px;">
                                                    <div class="col-lg-12 ">
                                                        <div class="control-group">
                                                            <div class="col-md-4">

                                                                <button type="submit" id="btnConPaciente" class="btn waves-effect waves-light btn-success">Actualizar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <br>

                                        <!-- form-group -->
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>






                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <?php
    include "plantilla/scriptfooter.php"
    ?>

    <script src="files/assets/node_modules/sweetalert/sweetalert.min.js"> </script>
    <script src="https://cdn.rawgit.com/alertifyjs/alertify.js/v1.0.10/dist/js/alertify.js"></script>
    <script src="../ajax/actEstadoPaciente.js"></script>


</body>

</html>