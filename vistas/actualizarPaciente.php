<!DOCTYPE html>
<?php session_start() ?>
<html lang="es">

<?php
include "plantilla/head.php"
?>

<body class="skin-blue fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/aside.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">Actualizar datos</h4>
                            <h6 class="card-subtitle"> </h6>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form class="input-form">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-group mb-3">
                                                    <input type="text" id="actualizarPacienteDocumento" class="form-control" placeholder="Buscar Documento" aria-label="" aria-describedby="basic-addon1">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-info" type="button" id="actualizarPacienteBuscar">Buscar...</button>
                                                    </div>
                                                </div>

                                                <div class="alert alert-danger" id="error" style="display:none">El usuario no se encuentra registrado en el sistema. </div>
                                            </div>
                                        </div>
                                        <br>

                                        <!-- form-group -->
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row" id="contForPaciente" style="display:block">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="m-b-0 text-dark">Formulario de Actualizar</h4>
                            </div>
                            <div class="card-body">
                                <form id="formActualizarPaciente" style="display:block" action="" method="post">
                                    <div class="form-body">
                                            <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Nombres</label>
                                            <div class="col-xs-6 col-md-9">
                                                <input type="text" placeholder="nombres" class="form-control " id="nombres" name="nombres">
                                                <small class="form-control-feedback"> </small> </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Apellidos</label>
                                            <div class="col-xs-6 col-md-9">
                                                <input type="text" placeholder="apellidos" class="form-control" id="apellidos" name="apellidos">
                                                <small class="form-control-feedback"> </small> </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Fecha de cumple años</label>
                                            <div class="col-xs-6 col-md-9 center">
                                                <input type="date" class="form-control" placeholder="dd/mm/yyyy" id="fnacimiento" name="fnacimiento">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">edad</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="edad" name="edad">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Direccion</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="direccion" name="direccion">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Numero de telefono</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="telefono" name="telefono">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="offset-sm-3 col-md-9">
                                                        <button type="button" id="actualizar" class="btn btn-success"> <i class="fa fa-check"></i>Actualizar</button>
                                                        <button type="button" onclick="resetFormulario()" class="btn btn-inverse">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>



                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <?php
    include "plantilla/scriptfooter.php"
    ?>
    <script src="files/assets/node_modules/sweetalert/sweetalert.min.js"> </script>
    <script src="https://cdn.rawgit.com/alertifyjs/alertify.js/v1.0.10/dist/js/alertify.js"></script>
    <!-- <script src="ajax/registrarPaciente.js"> </script> -->
    <script src="../ajax/actualizarPaciente.js"></script>
</body>

</html>