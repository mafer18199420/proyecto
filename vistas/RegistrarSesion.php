<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">

<?php
include "plantilla/head.php"
?>

<body class="skin-blue fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universidad de Santander</p>
        </div>
    </div>

    <div id="main-wrapper">
        <?php
        include "plantilla/nav.php";
        include "plantilla/aside.php";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- INICIO DEL CONTENIDO -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">Registrar Evolucion</h4>
                            <h6 class="card-subtitle"> </h6>
                            <form id="formA" name="formConSession" action="javascript:buscarTerapias()" method="post" autocomplete="off">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <form class="input-form">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="input-group mb-3">
                                                        <input class="form-control" id="documentoT" name="documentoT" type="number" required="required" data-validation-required-message="ingrese su documento">
                                                        <div class="input-group-append" id="divBoton" name="divBoton">
                                                            <button class="btn btn-info" type="submit" id="btnbuscarTerapia">Buscar...</button>
                                                        </div>
                                                    </div>
                                                    <div class="input-group row">
                                                        <label class="col-2 col-form-label">Terapias</label>
                                                        <div class="col-10">
                                                            <select id="idTerapia" name="idTerapia" class="custom-select col-12">
                                                                <option value=""> Selecione Terapia</option>

                                                            </select>
                                                        </div>
                                                    </div>



                                                </div>
                                                <br>
                                            </div>



                                        </form>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>




                <div class="row" id="contForPaciente" style="display:block">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="m-b-0 bg-info">Registrar evolucion</h4>
                            </div>
                            <div class="card-body">
                                <form id="formRegSesion" name="formRegPacientes" action="javascript: registrarSesion()" autocomplete="off">
                                    <div class="form-body">



                                        <div class="form-group row">
                                            <label class="col-2 col-form-label"> Nombre Responsable</label>
                                            <div class="col-xs-6 col-md-9">
                                                <input class="form-control" id="nombreR" name="nombreR" type="text" placeholder="Nombre del responsable" required="required" data-validation-required-message="ingrese su nombre">
                                                <small class="form-control-feedback"> </small>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">Descripcion</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" rows="5" id="descripcion" name="descripcion"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="offset-sm-3 col-md-9">
                                                        <input hidden name="idE" id="idE" value="<?php echo $_SESSION['id'] ?>">
                                                        <button class="btn btn-success" type="submit" id="btnSesion" name="btnBuscarRCat1"> <i class="fa fa-check"></i>Enviar</button>
                                                        <button type="button" class="btn btn-inverse">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- FIN DEL CONTENIDO -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Eliteadmin by themedesigner.in
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <?php
    include "plantilla/scriptfooter.php"
    ?>
    <script src="files/assets/node_modules/sweetalert/sweetalert.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.12.0/alertify.js"> </script>
    <script src="../ajax/registrarSesion.js"></script>
 
</body>

</html>