<!DOCTYPE html>
<?php session_start() ?>
<html lang="es">
<<?php include "seccion/adminT/head.php" ?> <body>
    <div id="wrapper">

        <!-- Navigation -->
        <?php include "seccion/adminT/aside.php" ?>
        <div id="page-wrapper">
            <div class="container-fluid">




                <div class="row">
                    <div class="col-lg-12">

                        <h1 class="page-header text-center">
                            Consultar Historia Clinica
                        </h1>

                    </div>
                </div>

                <form id="formConHistoria" name="formConHistoria" action="javascript:consultarHistorial()" autocomplete="off">

                    <div class="row">
                        <div class="col-lg-12  ">
                            <div class="col-md-4">
                                <label class="text">Documento</label>

                                <input class="form-control" id="documentoH" name="documentoH" type="number" required="required" data-validation-required-message="ingrese su nombre">
                                <p class="help-block text-danger"></p>
                            </div>


                        </div>



                    </div>

                    <div class="row" style="padding-top: 10px;">
                        <div class="col-lg-12 ">
                            <div class="control-group">
                                <div class="col-md-4">

                                    <button type="submit" id="btnConPaciente" class="btn btn-primary btn-lg ">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

                <br>
                <br>
                <div class="row">

                    <div class="col-lg-10 col-md-offset-1">

                        <div class="table-responsive">
                            <table id="tableHistoria" name="tableHistoria" class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>idMotivo</th>
                                        <th>Motivo</th>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>Fecha Registro</th>
                                        <th>Observacion</th>

                                    </tr>
                                </thead>
                                <tbody id="bodyH">


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>




            </div>



        </div>





        <div id="myModalVerSesion" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="text-align: center" class="modal-title"> Informacion Intervencion </h4>
                    </div>
                    <div style="text-align: center" class="modal-body">


                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Responsable</th>
                                        <th>Descripcion</th>
                                        <th>Fecha Registro </th>

                                    </tr>
                                </thead>
                                <tbody id="bodyS">


                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="modal-footer">

                        <div class="form-group">
                            <div class="col-md-3 col-md-offset-4">

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>


        <div id="myModalVerTerapia" name="myModalVerTerapia" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="text-align: center" class="modal-title"> Informacion Evolucion </h4>
                    </div>
                    <div style="text-align: center" class="modal-body">

                        <div style="text-align: center" class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Responsable</th>
                                        <th>Descripcion</th>
                                        <th>Cantidad de sesion</th>
                                        <th>Anvance</th>
                                        <th>Estado</th>
                                        <th>Fecha Inicio</th>

                                    </tr>
                                </thead>
                                <tbody id="bodyTe">


                                </tbody>
                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                        <div class="form-group">
                            <div class="col-md-3 col-md-offset-4">

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>


        <div id="myModalVerInforme" name="myModalVerInforme" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="text-align: center" class="modal-title"> Informacion Valoracion </h4>
                    </div>
                    <div style="text-align: center" class="modal-body">

                        <div style="text-align: center" class="table-responsive">
                            <table class="table  table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>CARACTERISTICA</th>
                                        <th>RECOMENDACION</th>

                                    </tr>
                                </thead>
                                <tbody id="bodyT">




                                </tbody>
                            </table>

                        </div>


                    </div>
                    <div class="modal-footer">

                        <div class="form-group">
                            <div class="col-md-3 col-md-offset-4">

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>



    </div>




    </div>

    <?php include "seccion/adminT/script.php" ?>
    </body>

</html>