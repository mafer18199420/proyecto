function resetFormulario() {
    $("#formActualizarUsuario")[0].reset();
}




$("#actualizarU").on("click", function () {



    if ($("#nombres").val() == '' || $("#apellidos").val() == '' || $("#direccion").val() == '' ||
        $("#telefono").val() == '' || $("#email").val() == '' || $("#fnacimiento").val() == '' ||
        $("#contra").val() == '' || $("#contraN").val() == '') {


        swal("Error", "todos los datos del paciente son requeridos", "error");
        return;

    }




    if ($("#contra").val() != $("#conContra").val()) {


        swal("Error", "La contraseña no coincide", "error");
        return;


    }


    let nom = $("#nombres").val();
    let ape = $("#apellidos").val();
    let dire = $("#direccion").val();
    let tel = $("#telefono").val();
    let email = $("#email").val();
    let fechaNaci = $("#fnacimiento").val();
    let contra = $("#contra").val();
    let id = $("#ide").val();
    let opcion = 10;


    $.ajax({

        url: '../controladores/ControladorPaciente.php',
        type: 'POST',
        data: { nom, ape, dire, tel, email, fechaNaci, contra, id, opcion },
        success: function (response) {

            console.log(response);

            if (response == '0') {
                 
             
                swal("Error", "El usuario no se encuentra registrado ", "error");
                return;
              

            } else {

                $("#formActualizarUsuario")[0].reset();
                swal("Buen trabajo", "Datos actualizados", "success");
                return;


                // alertify.logPosition("bottom right");
                // alertify.success("Usuario Registrado");
            }

        }

    })


});



$("#actualizarU").on("click", function () {

    let id = $("#ide").val();
    let opcion = 11;

    $.ajax({

        url: '../controladores/ControladorPaciente.php',
        type: 'POST',
        data: { id, opcion },
        success: function (response) {

            let datos = JSON.parse(response);

            console.log(datos);


            if (datos.opcion == 1) {

                $("#error").css({
                  "display":"none"
                });
        
                $("#nombres").val(datos.data.nombres);
                $("#apellidos").val(datos.data.apellidos);
                $("#direccion").val(datos.data.direccion);
                $("#telefono").val(datos.data.telefono);
                $("#fnacimiento").val(datos.data.fechaNacimiento);
                $("#email").val(datos.data.email);
                $("#contra").val(datos.data.contra);
                $("#conContra").val(datos.data.contra);


                
        
              } else if(datos.opcion == 2 ){
                  $("#error").css({
                    "display":"block"
                  });
                  resetFormulario();
              }



        }

    })






});



