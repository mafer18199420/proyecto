
//Funcion buscar Paciente para el actualizar
$("#actualizarPacienteBuscar").on("click", function () {
  var documento = $("#actualizarPacienteDocumento").val();
  let opcion = 8;

  if (documento != "") {
    $.ajax({

      url: '../controladores/ControladorPaciente.php',
      type: 'POST',
      data: { documento, opcion },
      success: function (response) {
        let datos = JSON.parse(response);

       console.log(datos);
      if (datos.opcion == 1) {

        $("#error").css({
          "display":"none"
        });

        $("#nombres").val(datos.data.nombres);
        $("#apellidos").val(datos.data.apellidos);
        $("#fnacimiento").val(datos.data.fechaNacimiento);
        $("#edad").val(datos.data.edad);
        $("#direccion").val(datos.data.direccion);
        $("#telefono").val(datos.data.telefono);
        

      } else if(datos.opcion == 2 ){
          $("#error").css({
            "display":"block"
          });
          resetFormulario();
      }

      }

    })
  }
});

function resetFormulario(){
  $("#formActualizarPaciente")[0].reset();
}

$("#actualizar").on("click",function() {



  if ($("#nombres").val() == '' || $("#apellidos").val() == '' || $("#direccion").val() == '' ||
    $("#telefono").val() == '' || $("#edad").val() == '' || $("#ndocumento").val() == ''|| $("#fnacimiento").val() == '') {

      swal("Error", "todos los datos del paciente son requeridos", "error");
      return;
    // alertify.logPosition("bottom right");
    // alertify.error("Error - Faltan Datos");

  }
  let documento = $("#actualizarPacienteDocumento").val();
  let nom = $("#nombres").val();
  let ape = $("#apellidos").val();
  let fechaN = $("#fnacimiento").val();
  let dire = $("#direccion").val();
  let tel = $("#telefono").val();
  let edad = $("#edad").val();

  let opcion = 3;


  $.ajax({

    url: '../controladores/ControladorPaciente.php',
    type: 'POST',
    data: { documento,nom, ape, dire, tel, edad,fechaN, opcion },
    success: function (response) {

        console.log(response);

        if (response !='') {
          
     
          $("#formActualizarPaciente")[0].reset();
          swal("Buen trabajo", "Datos del Usuario Registrado", "success");
          return;
        } else{

          swal("Error", "El usuario no se a podido registrado ", "error");
          return;
        }



    }

  })


});