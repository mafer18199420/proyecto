//  funcion para buscar paciente(usuario)

$("#registrarPacienteBuscar").on("click", function () {
    var documento = $("#registrarPacienteDocumento").val();
    let opcion = 14;

    if (documento != "") {
        $.ajax({

            url: '../controladores/ControladorPaciente.php',
            type: 'POST',
            data: { documento, opcion },
            success: function (response) {

                // console.log(response);

                if (parseInt(response) == 1) {
                    //muestra el formulario si l respuesta es 1 (no existe usuario)
                    $('#dataConsulta').html("");
                    swal("Error", "El usuario no se encuentra registrado ", "error");
                    return;


                } else if (parseInt(response) == 2) {
                    //respuesta cuando el usuario no le pertenece al terapeuta si 2 respuesta 
                    $('#dataConsulta').html("");
                    swal("Error", "Usted no tiene asignado este usuario ", "error");
                    return;


                } else {
                    //Imprime el resultado

                    $('#dataConsulta').html(response);


                }

            }

        })
    }
});


let seleccionado;

$("#tipoValoracion").on("click", function () {

    let opcion = 1;
    let valoracion = $("#tipoValoracion option:selected").val();
    //   console.log(valoracion);
    if (valoracion == "" || seleccionado == valoracion) {
        return;
    }
    seleccionado = valoracion;

    $.ajax({

        url: '../controladores/ControladorPreguntas.php',
        type: 'POST',
        data: { valoracion, opcion },
        success: function (response) {
            // console.log(response);

            if (parseInt(response) == 1) {
                //muestra el formulario si l respuesta es 1 (no existe usuario)
                $('#preguntas').html("");
                swal("Error", "El usuario no se encuentra registrado ", "error");
                return;


            } else {
                //Imprime el resultado

                $('#preguntas').html(response);


            }

        }

    })

});





function prueba(idRe) {


    let id_recomendacion = idRe;
    let id_pregunta = $('#idPregunta').val();


    $('#preguntas tr').each(function (i, d) {
        // console.log($($(this).children('tr td')[0]).text());
        if ($($(this).children('tr td')[0]).text() == id_pregunta) {

            let a = $($(this).children('tr td')[3]).text(id_recomendacion);

            // console.log(a);


            let b = $($(this).children('tr td')[3]).text();
            console.log(b);

        }

    });




}




function mostrarModal(id_p, valor) {


    let id_pregunta = $('#idPregunta').val(id_p);


    console.log(valor);
    let opcion = 3;

    if (valor == 1) {

        $.ajax({
            url: '../controladores/ControladorPreguntas.php',
            type: 'POST',
            data: { id_p, opcion },
            success: function (response) {

                // console.log(response);

                $('.modal-body').empty();
                $(".modal-body").append(`<tr></tr>`);

                let data = JSON.parse(response);

                console.log(data);

                $.each(data, function (i, p) {
                    var nuevafila = "<tr><td style=display:none>" +
                        p[i].idRe + "</td><td>" +
                        p[i].descripcion + "</td><td>" +
                        `<button class="btn btn-success" id="guardarId" onclick="prueba( ${p[i].idRe} )"> Agregar</button>` +
                        "</td>" + "</tr>";

                    $(".modal-body").append(nuevafila);

                });

                $("#pruebaModal").modal('show');

            }

        })

    } else if (valor == 2) {



        $('#preguntas tr').each(function (i, d) {
            // console.log($($(this).children('tr td')[0]).text());
            if ($($(this).children('tr td')[0]).text() == id_pregunta) {

                let a = $($(this).children('tr td')[3]).text('');
                console.log(a);

            }

        });


    }







}


$('#guarRespuesta').on('click', function () {

    let nFilas = $("#preguntas tr").length;
    let contador = 0;
    let estado = false;


    $("#preguntas .resRadio").each(function (indice, elemento) {

        if ($('input:radio[name=opt' + indice + ']:checked').attr('value') === undefined && contador < nFilas) {

            swal("Error", "Todas las valoraciones son requeridas ", "error");
        } else {

            if (contador == (nFilas - 1)) {

                estado = true;
            }
        }



        contador++;


    });

    if (estado === true) {


        var header = Array();
        $("#preguntas tr th").each(function (i, v) {
            header[i] = $(this).text();
        });

        // creo un array
        var data = Array();
        //donde tenga id pregunta , each para recorrer 
        $("#preguntas tr").each(function (i, v) {
            data[i] = Array();
            //this elemento seleccionada el hijo que sea td recoralo(i=indice, v= guarda el resultado del recorrido)
            $(this).children('td').each(function (ii, vv) {

                // si indices es igual 0 entre
                if (ii == 0) {
                    // trae el id de la pregunta y 
                    data[i][0] = $(this).text();
                    // este valida radio y toma el value del seleccionado
                    data[i][1] = $('input:radio[name=opt' + i + ']:checked ').attr('value');




                }

            });

        });





        // console.log(data);
        // INFO 	= new FormData();
        var documento = $("#registrarPacienteDocumento").val();
        let idE = $("#idEncargado").val();


        //    console.log(idE+"es mi encargado");
        //    console.log(documento+"es mi documento");
        let opcion = 2;
        let aInfo = JSON.stringify(data);

        console.log(aInfo);

        let valoracion = $("#tipoValoracion option:selected").val();
        console.log(valoracion);

        // INFO.append('data', aInfo);

        $.ajax({
            data: { aInfo, documento, valoracion, idE, opcion },
            type: 'POST',
            url: '../controladores/ControladorPreguntas.php',
            success: function (response) {
                console.log(response);


                if (response == '') {
                    swal("Error", "No se pudo registrar  ", "error");
                    return;
                    
                } else {
                    swal("buen trabajo", "Valoracion  Registrada", "success");
                    return;
                     
                 } 

              


            }
        })



        return;





    }



});


$('#guarRespuesta').on('click', function () {
    // event.preventDefault();

    var data = Array();

    $('#preguntas tr').each(function (i, d) {


        let a = $($(this).children('tr td')[0]).text();
        let b = $($(this).children('tr td')[3]).text();
        console.log(a);
        console.log(b);

        if (a == b) {

            data[i] = Array(b, a);
            console.log(data[i]);
        }





    });


    let aInfo2 = JSON.stringify(data);
    let valor = $('#preguntas .resRadio').val();


    let opcion = 4;
    console.log(aInfo2);



    $.ajax({
        data: { aInfo2, valor, opcion },
        type: 'POST',
        url: '../controladores/ControladorPreguntas.php',
        success: function (response) {
            console.log(response);




        }
    })



    $('#mitabla tbody').empty()
});