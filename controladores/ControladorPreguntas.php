<?php


include_once(__DIR__ . '/../modelos/ModeloPreguntas.php');
include_once(__DIR__ . '/../modelos/ModeloHistoria.php');
$controlador = new ControladorPreguntas();


$opcion = 0;




if (isset($_POST['opcion'])) {
  $opcion = $_POST['opcion'];


  switch ($opcion) {

    case 1:
      // modificad por we
      $result = $controlador->listarPreguntas();
      break;
    case 2:
      // modificad por we
      $result = $controlador->traerRepuesta();
      break;

    case 3:
      // modificad por we
      $result = $controlador->buscarRecomendacion();
      break;

    case 4:
      $result = $controlador->recomendacionID();
      break;
  }
}



class ControladorPreguntas
{

  public $modelo;
  public $modeloH;




  public function __construct()
  {

    $this->modelo = new ModeloPreguntas();
    $this->modeloH = new ModeloHistoria();
  }

  public function listarPreguntas()
  {

    $val = $_POST['valoracion'];
    return $this->modelo->listarPreguntas($val);
  }
  public function traerRepuesta()
  {

    $documento = $_POST['documento'];
    $val= $_POST['valoracion'];
    $idE =$_POST['idE'];
    $DATA2 = json_decode($_POST['aInfo']);


    $hoy = getdate();

    $Dhoy = $hoy['mday'];
    $Mhoy = $hoy['mon'];
    $Ahoy = $hoy['year'];

    $fechaSistema = $Ahoy . '-' . $Mhoy . '-' . $Dhoy;



    $respuestas = $this->modelo->traerRepuesta($documento, $fechaSistema, $val,$idE ,$DATA2);

  
    // var_dump($respuestas);
   if($respuestas ==! '0' ){

    // var_dump($respuestas.'hello');
    $regHistoria = $this ->modeloH -> regHistoria($documento , $val , $fechaSistema);


    
   }
 




    return $respuestas;
  }

  public function buscarRecomendacion()
  {
    $id_p = $_POST['id_p'];

    return $this->modelo->buscarRecomendacion($id_p);
  }

  public function recomendacionID()
  {
    $DATA = json_decode($_POST['aInfo2']);
    $valor = $_POST['valor'];
 

    $Recome = $this->modelo->recomendacionID($DATA,$valor);

    return $Recome;
  }
}
