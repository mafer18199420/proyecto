<?php
include_once(__DIR__.'/../modelos/ModeloLogin.php'); 

$usuario = $_POST['usuario'];
$pass = $_POST['clave'];

$datos = [
  "u"=> $usuario,
   "p"=> $pass
];
   
 $controlador = new ControladorLogin($datos);
 $result = $controlador->iniciarSesion();

class ControladorLogin{

public $datos;
public $modelo;

public function __construct($datos){
  $this->datos = $datos;
  $this->modelo = new ModeloLogin();
}

 public function iniciarSesion(){
    $res = $this->modelo->iniciar_sesion_modelo($this->datos);
    echo json_encode($res);
    // var_dump($res);
    return $res;
  }
}
