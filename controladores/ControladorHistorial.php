<?php


include_once(__DIR__ . '/../modelos/ModeloPreguntas.php');
include_once(__DIR__ . '/../modelos/ModeloHistoria.php');

$controladorh = new ControladorHistorial();

// $controladorh = new Controlador


$opcion = 0;




if (isset($_POST['opcion'])) {
  $opcion = $_POST['opcion'];


  switch ($opcion) {

    case 1:
      $result = $controladorh->mostrarValoracion();
      break;

    case 2:
      $result = $controladorh->mostrarTerapia();
      break;

    case 3:
      $result = $controladorh->mostrarSesion();
      break;
  }
}



class ControladorHistorial
{

  public $modelo;
  public $modeloH;




  public function __construct()
  {

    $this->modelo = new ModeloPreguntas();
    $this->modeloH = new ModeloHistoria();
  }





  public function mostrarValoracion()
  {
    $data = $_POST['datos'];


    $valoracion = $this->modeloH->mostrarValoracion($data);

    return $valoracion;
  }

  public function mostrarTerapia()
  {

    $data = $_POST['datos'];

    $terapiaMostrar = $this->modeloH->mostrarTerapia($data);

    return $terapiaMostrar;
  }

  public function mostrarSesion()
  {
   
    $data = $_POST['datos'];

    $mostrarSesion = $this->modeloH->mostrarSesion($data);

    return $mostrarSesion;
    

  }
}
