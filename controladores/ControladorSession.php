<?php
include_once(__DIR__.'/../modelos/ModeloLogin.php'); 
   

class ControladorSession{

// public $modelo;

public function __construct(){
    // $this->modelo = new ModeloLogin();
}


    public function verificarSesion(){

        @session_start();
         
        if(empty($_SESSION)){
            header("Location: ../index.php");
        }
    }
  
    public function cerrarSesion(){
        @session_start();
        
        $_SESSION = array();
        
        header("Location: ../index.php");
    }
  
}