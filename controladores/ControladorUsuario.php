<?php



include_once(__DIR__ . '/../modelos/ModeloUsuario.php');




$controlador = new ControladorUsuario();

$opcion = 0;

if (isset($_POST['opcion'])) {
  $opcion = $_POST['opcion'];

  switch ($opcion) {

    case 1:

      $documento = $_POST['documento'];
      $nombres = $_POST['nombres'];
      $apellidos = $_POST['apellidos'];
      $direccion = $_POST['direccion'];
      $telefono = $_POST['telefono'];
      $email = $_POST['email'];
      $fnacimiento = $_POST['fnacimiento'];
     

      $datos = [
        "documento" => $documento,
        "nombres" => $nombres,
        "apellidos" => $apellidos,
        "direccion" => $direccion,
        "telefono" => $telefono,
        "email" => $email,
        "fnacimiento" => $fnacimiento   
      ];


      $result = $controlador->regUsuario($datos);
      break;

    case 2:
    $result = $controlador-> buscarUsuarios();
      break;
    
    case 3:
     
      $result = $controlador->validarContra();
   
      break;
    case 4:
    
    $id = $_POST['id'];
    $result = $controlador-> actRegistroUsuario($id);
    break;

    case 5:

    $result = $controlador-> actUsuarioContra();
    break;
    
  }
}


class ControladorUsuario
{

  public $modelo;
  public $modeloU;

  public function __construct()
  {

    $this->modelo = new ModeloUsuario();

  }



  public function regUsuario($datos)
  {

    $hoy = getdate();

    $Dhoy = $hoy['mday'];
    $Mhoy = $hoy['mon'];
    $Ahoy = $hoy['year'];

    $fechaSistema = $Ahoy . '-' . $Mhoy . '-' . $Dhoy;
    return $this->modelo->registrarUsuario($datos, $fechaSistema);
  }


  public function buscarUsuarios(){

    return $this->modelo -> listarUsuarios();
  }
 
   public function actRegistroUsuario($id){
    return $this ->modelo -> actRegistroUsuarioModelo($id);
   }
   public function validarContra()
   {
    $usuario = $_POST['usuario'];
    return $this->modelo -> validarUsuarioModelo($usuario);
   }

   public function  actUsuarioContra()
   {
    $usuario = $_POST['usu'];
    $documento = $_POST['docu'];
    $contra = $_POST['contra'];

    
     $datos = [
    
    "usuario"=> $usuario,
    "documento"=> $documento,
    "contra"=> $contra
  
    ];
    return $this->modelo -> actUsuarioContraModelo($datos);
   }

}
