<?php



include_once(__DIR__ . '/../modelos/ModeloTerapia.php');
include_once(__DIR__ . '/../modelos/ModeloPaciente.php');
include_once(__DIR__ . '/../modelos/ModeloInforme.php');
include_once(__DIR__ . '/../modelos/ModeloHistoria.php');
include_once(__DIR__ . '/../modelos/ModeloRecomendacion.php');


$controlador = new ControladorTerapia();

$opcion = 0;

if (isset($_POST['opcion'])) {
  $opcion = $_POST['opcion'];

  switch ($opcion) {

    case 1:




      $result = $controlador->regTerapia();
      break;

    case 2:

      $documento = $_POST['documentoT'];
      $result = $controlador->consultarTerapias($documento);


      break;

    case 3:

      $idT = $_POST['idT'];


      $result = $controlador->consultarAvance($idT);
      break;

    case 4:

      $idT = $_POST['idT'];
      $res = $_POST['responsable'];
      $des = $_POST['des'];

      $datos = [

        "idT" => $idT,
        "responsable" => $res,
        "des" => $des

      ];

      $result = $controlador->regSesion($datos);
      break;


    case 5:

      $idS = $_POST['idS'];
      $result = $controlador->consultarSesion($idS);
      break;
  }
}


echo json_encode($result);


class ControladorTerapia
{

  public $modelo;
  public $modeloP;
  public $modeloH;


  public function __construct()
  {

    $this->modelo = new ModeloTerapia();
    $this->modeloP = new ModeloPaciente();
    $this->modeloH = new ModeloHistoria();
  }


  public function regTerapia()
  {

    $documento = $_POST['documento'];
    $responsable = $_POST['responsable'];
    $cantSesion = $_POST['cantSesion'];
    $fechaInicio = $_POST['fechaInicio'];
    $descripcion = $_POST['descripcion'];


    $datos = [

      "documento" => $documento,
      "responsable" => $responsable,
      "cantSesion" => $cantSesion,
      "fechaInicio" => $fechaInicio,
      "descripcion" => $descripcion

    ];


    // $hoy = getdate();

    // $Dhoy = $hoy['mday'];
    // $Mhoy = $hoy['mon'];
    // $Ahoy = $hoy['year'];

    // $fechaSistema = $Ahoy . '-0' . $Mhoy . '-0' . $Dhoy;

    date_default_timezone_set('America/Bogota');
    setlocale(LC_TIME, 'es_MX.UTF-8');
    $fecha_actual = strftime("%Y-%m-%d");



    $idTerapia = $this->modelo->regModeloTerapia($datos, $fecha_actual);

      return $idTerapia;
  }


  public function regSesion($datos)
  {


 

    date_default_timezone_set('America/Bogota');
    setlocale(LC_TIME, 'es_MX.UTF-8');
    $fecha_actual = strftime("%Y-%m-%d");





    $idSesion = $this->modelo->regModeloSesion($datos, $fecha_actual);


    // $motivo = "Sesion";
    // $terapias = $this->modelo->buscarTerapiaM($datos['idT']);
    // $regHistoria = $this->modeloH->regHistoria($terapias[0]['idP'], $motivo, $idSesion);
   
    if($idSesion ==! 0){

    $actA = $this->modelo->actualizarTerapicaA($datos['idT']);

    $actE = $this->modelo->actualizarTerapicaE($datos['idT']);
    }

    return  $idSesion ;
  }


  public function consultarAvance($idT)
  {


    $terapias = $this->modelo->buscarTerapiaM3($idT);

    return $terapias;
  }


  public function consultarSesion($idS)
  {


    $terapias = $this->modelo->buscarSesionM($idS);

    return $terapias;
  }

  //consultar terapias para cargarla en la vista registrarSesion

  public function consultarTerapias($documento)
  {


    $terapias = $this->modelo->buscarTerapiasM($documento);

    return $terapias;
  }
}
