<?php

include_once(__DIR__.'/../modelos/ModeloTipoValoracion.php'); 
    


class ControladorTipoValoracion{

public $modelo;


public function __construct(){
  
  $this->modelo = new ModeloTipoValoracion();
  
}

 public function tiposValoracion(){

  return $this ->modelo ->tiposValoracion();
 }
}