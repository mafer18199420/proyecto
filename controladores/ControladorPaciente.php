<?php


include_once(__DIR__ . '/../modelos/ModeloPaciente.php');
include_once(__DIR__ . '/../modelos/ModeloHistoria.php');

$modelo = new ModeloPaciente();
$modeloH = new ModeloHistoria();


$controlador = new ControladorPaciente();


$opcion = 0;


if (isset($_POST['opcion'])) {
  $opcion = $_POST['opcion'];


  switch ($opcion) {

    case 1:
      // modificad por we
      $result = $controlador->regPaciente();
      break;

    case 2:
      // modificada por we
      $result = $controlador->buscarPaciente();
      break;

    case 3:


      $result = $controlador->actPaciente();


      break;


    case 4:

      $documento = $_POST['documento'];
      $estado = $_POST['estado'];


      $datos = [

        "documento" => $documento,
        "estado" => $estado
      ];

      $result = $controlador->actEstadoPaciente($datos);


      break;



    case 5:

      // Modificada we
      $result = $controlador->listarPacientes($idEncargado);


      break;


    case 6:

      //Modificado we
      $result = $controlador->agregarPacTerap();


      break;

    case 7:

      $documento = $_POST['documento'];
      $result = $controlador->buscarHistoria($documento);


      break;


    case 8:

        // Modificado para buscarDatos
        $result = $controlador-> buscarDatosPaciente();


      break;


    case 9:


      $correo = $_POST['correo'];
      $result = $controlador->actRegistroUsuario($correo);
      break;



    case 10:

     $result = $controlador->actUsuario();


      break;


    case 11:

      $result = $controlador->actUsuarioEncargado();



      break;


    case 12:

      $usuario = $_POST['usu'];
      $documento = $_POST['docu'];
      $contra = $_POST['contra'];


      $datos = [

        "usuario" => $usuario,
        "documento" => $documento,
        "contra" => $contra

      ];

      $result = $controlador->actUsuarioContra($datos);


      break;


    case 13:

      $nombres = $_POST['nombres'];
      $apellidos = $_POST['apellidos'];
      $documento = $_POST['documento'];
      $telefono = $_POST['telefono'];
      $direccion = $_POST['direccion'];
      $fnacimiento = $_POST['fnacimiento'];
      $email = $_POST['email'];

      $datos = [

        "nombres" => $nombres,
        "apellidos" => $apellidos,
        "documento" => $documento,
        "telefono" => $telefono,
        "direccion" => $direccion,
        "fnacimiento" => $fnacimiento,
        "email" => $email

      ];


      $result = $controlador->regUsuarioP($datos);

      
      break;
      case 14;

      $result = $controlador->buscarUsuarioValoracion();

      break;
  }
}












class ControladorPaciente
{

  public $modelo;
  public $modeloP;
  public $modeloH;



  public function __construct()
  {

    $this->modelo = new ModeloPaciente();
    $this->modeloH= new ModeloHistoria();
  }


  public function regUsuarioP($datos)
  {

    $hoy = getdate();

    $Dhoy = date("d");;
    $Mhoy = date("m");
    $Ahoy = $hoy['year'];

    $fechaSistema = $Ahoy . '-' . $Mhoy . '-' . $Dhoy;
    return $this->modelo->regModeloUsuarioP($datos, $fechaSistema);
  }


  public function buscarPaciente()
  {
    $documento = $_POST['documento'];
    $respuesta = $this->modelo->buscarPaciente($documento);
    echo $respuesta;
  }



  public function buscarUsuarios()
  {

    return $this->modelo->buscarUsuariosModelo();
  }


  public function buscarHistoria($documento)
  {


    $historia=$this->modeloH->buscarHistoriaModelo($documento);
    
    return $historia;
  }


  public function regPaciente()
  {

    $nombreR = $_POST['nom'];
    $apellidoR = $_POST['ape'];
    $ndocumentoR = $_POST['ndoc'];
    $telefonoR = $_POST['tel'];
    $direccionR = $_POST['dire'];
    $fnacimientoR = $_POST['fna'];
    $edadR = $_POST['edad'];
    $idEncargado = $_POST['idEncargado'];

    if($edadR>='60'){

      $datos = [

        "nombreR" => $nombreR,
        "apellidoR" => $apellidoR,
        "ndocumentoR" => $ndocumentoR,
        "telefonoR" => $telefonoR,
        "direccionR" => $direccionR,
        "fnacimientoR" => $fnacimientoR,
        "edadR" => $edadR,
        "idEncargado" => $idEncargado
  
  
      ];
  
  
      return $this->modelo->regModeloPaciente($datos);

    }else{
       return;
    }

   
  }


  public function actPaciente()
  {
    $documento = $_POST['documento'];
    $nombreA = $_POST['nom'];
    $apellidoA = $_POST['ape'];
    $fechaNacim = $_POST['fechaN'];
    $telefonoA = $_POST['tel'];
    $direccionA = $_POST['dire'];
    $edadA = $_POST['edad'];


    $datos = [
      "documento"=>$documento,
      "nombreA" => $nombreA,
      "apellidoA" => $apellidoA,
      "fechaN"     => $fechaNacim,
      "telefonoA" => $telefonoA,
      "direccionA" => $direccionA,
      "edadA" => $edadA

    ];

    return $this->modelo->actPacienteModelo($datos);
  }


  public function actUsuario()
  {
     $nombreU = $_POST['nom'];
    $apellidoU = $_POST['ape'];
    $direccionU = $_POST['dire'];
    $telefonoU = $_POST['tel'];
    $fechaNacimU = $_POST['fechaNaci'];
     $emailU = $_POST['email'];
     $contraU = $_POST['contra'];
     $id = $_POST['id'];

     $datos = [
      
      "nombres" => $nombreU,
      "apellidos" => $apellidoU,
      "direccion" => $direccionU,
      "telefono" => $telefonoU,
      "fnacimiento" => $fechaNacimU,
      "email" => $emailU ,
      "contra" => $contraU,
      "ide"=> $id
    ];



    return $this->modelo->ActualizarDatosTerapeuta($datos);
  }


  public function actUsuarioContra($datos)
  {
    return $this->modelo->actUsuarioContraModelo($datos);
  }



  public function actEstadoPaciente($datos)
  {
    return $this->modelo->actEstadoPacienteModelo($datos);
  }

  public function actRegistroUsuario($correo)
  {
    return $this->modelo->actRegistroUsuarioModelo($correo);
  }


  public function conPaciente($datos)
  {
    return $this->modelo->conPacienteModelo($datos);
  }

  // Lista todos los pacientes segun el encargado
  public function listarPacientes($idEncargado)
  {

    return $this->modelo->listarPacienteModelo($idEncargado);
  }

  public function agregarPacTerap()
  {

    $idEncargado = $_POST['idencargado'];
    $idPaciente = $_POST['idPaciente'];

    $datos = [

      "idPac" => $idPaciente,
      "idEncargado" => $idEncargado
    ];

  
    return $this->modelo->agregarPacTerap($datos);
  }

  public function buscarDatosPaciente(){

    $documento = $_POST['documento'];
    $respuesta = $this->modelo->buscarDatosPaciente($documento);
    echo $respuesta;
  }

  public function buscarUsuarioValoracion(){
    session_start();
    $terapeuta= $_SESSION['id'];
    $documento = $_POST['documento'];
    
    $respuesta = $this->modelo->buscarUsuarioValoracion($documento,$terapeuta);
    echo $respuesta;
  }


  public function actUsuarioEncargado(){

    $id = $_POST['id'];

  
    $respuesta = $this->modelo->buscarDatosEncargado($id);
    echo $respuesta;


  }
}
