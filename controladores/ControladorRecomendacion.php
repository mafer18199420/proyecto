<?php


include_once(__DIR__ . '/../modelos/ModeloPaciente.php');
include_once(__DIR__ . '/../modelos/ModeloInforme.php');
include_once(__DIR__ . '/../modelos/ModeloHistoria.php');
include_once(__DIR__ . '/../modelos/ModeloRecomendacion.php');

$controlador = new ControladorRecomendacion();


$opcion = 0;




if (isset($_POST['opcion'])) {
  $opcion = $_POST['opcion'];

  switch ($opcion) {

    case 1:

      // $idI = $_POST['idI'];

      // $result = $controlador->controBuscarRecoCat1($idI);
      // break;

    case 2:

      $documento = $_POST['documentoR'];
      $result = $controlador->consultarValoracion($documento);
      break;

    case 3:

      $result = $controlador->consultarRecomendacion();
      break;

    case 4:

      $result = $controlador->buscarGrafica();
      break;

    case 5:

      $result = $controlador->buscarGraficaGeneral();
      break;
  }
}


echo json_encode($result);


class ControladorRecomendacion
{

  public $modeloI;
  public $modeloP;
  public $modeloR;
  public $modeloH;



  public function __construct()
  {
    $this->modeloP = new ModeloPaciente();
    $this->modeloH = new ModeloHistoria();
    $this->modeloR = new ModeloRecomendacion();
  }




  // public function controBuscarRecoCat1($idI)
  // {

  //   $respuesta = $this->modeloR->buscarRecoModeloCat_1($idI);
  //   return $respuesta;
  // }


  public function consultarValoracion($documento)
  {
    session_start();
    $terapeuta = $_SESSION['id'];

    $recomendacion = $this->modeloR->buscarValoracion($documento, $terapeuta);



    return $recomendacion;
  }

  public function consultarRecomendacion()
  {
    $valoracion = $_POST['valoracion'];



    $recomendacion = $this->modeloR->buscarRecomendacion($valoracion);

    return $recomendacion;
  }

  public function buscarGrafica()
  {

    $valoracion = $_POST['valoracion'];

    $grafica = $this->modeloR->buscarGrafica($valoracion);

    return $grafica;
  }


  public function buscarGraficaGeneral()
  {
    session_start();
    $terapeuta = $_SESSION['id'];

    $valoracion = $_POST['idC'];
    $grafica = $this->modeloR->buscarGraficaGeneral($valoracion,$terapeuta);

    return $grafica;
  }
}
